<?php
	include "database.php";
	include "function.php";
	session_start();
	if(!isset($_SESSION["AID"]))
	{
	header('Location: admin_login.php');

		// echo "<script>window.open('admin_login.php','_self')</script>";
	}
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
     <?php include "head.php"; ?>
    </head>

    <body>
        <nav class="navbar navbar-default">
            <div class="">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mynavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>

            </button>
                    <a href="index.php" class="navbar-brand text-uppercase" style="color:white;">
                        <img src="img/logorect.png" class="img-responsive">
                    </a>
                </div>
                <div id="mynavbar" class="collapse navbar-collapse text-uppercase">
                    <ul class="nav navbar-nav navbar-right ">
					
                        <li><a href="" class="active">Hi...<?php echo $_SESSION["ANAME"]; ?></a></li>
                         
  <li><a href="admin_view_stud.php"><span class="fa fa-users"></span> Student</a></li>
  <li><a href="admin_add_book.php"> <span class="fa fa-upload"></span> Books</a></li>
  <li><a href="upload_books_link.php"> <span class="fa fa-cloud-upload"></span> Link</a></li>
  <li><a href="admin_view_book.php"><span class="fa fa-book"></span> Books</a></li>
  <li><a href="admin_view_req.php"><span class="fa fa-paper-plane"></span> Request</a></li>
  <li><a href="admin_view_comments.php"> <span class="fa fa-comments"></span> Comments</a></li>
                        <li><a href="student_change.php"><span class="fa-passwd-reset fa-stack">
                                                         <i class="fa fa-undo fa-stack-2x"></i>
                                                            <i class="fa fa-lock fa-stack-1x"></i>
                                                                                        </span> Change Password</a></li>
                        <li><a href="logout.php"><span class="fa fa-power-off"></span> Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- body -->
        




<div class="container">
    <div class="row well outer">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <table class="table table-stripped text-white">


			<tr> <th>Total Students : </th><td> <?php echo countRecord("SELECT * from student",$db); ?></td> </tr>
			<tr>	<th>Total Books    : </th><td><?php echo countRecord("SELECT * from book",$db); ?></td> </tr>
			<tr> <th>Total Request  : </th><td><?php echo countRecord("SELECT * from request",$db); ?></td> </tr>
			<tr> <th>Total Comments : </th><td><?php echo countRecord("SELECT * from comment",$db); ?></td> </tr>
		</table>


        </div>
        <div class="col-sm-2"></div>
    </div>
</div>

<div class="container">
    <div class="row well outer">
        <div class="col-sm-2"></div>
        <div class="col-sm-8  text-white">
<h3 class="text-uppercase">Upload Books</h3>
<?php
	if(isset($_POST["submit"]))
		{
			$bname=$_POST["bname"];
			$keys=$_POST["keys"];
			$author=$_POST["author"];
			$category=$_POST["category"];
		$link=$_POST["link"];
				$sql="INSERT INTO book(BTITLE,KEYWORDS,CATEGORY,AUTHOR,FILE)
                     VALUES ('{$bname}','{$keys}','{$category}','{$author}','{$link}')";
  
                     if($db->query($sql))
				{
				echo "<p class='text-white'>Book Added Successfully...</p>";
				}
				else
				{
				echo "<p class='text-white'>Book Adding Failed.</p>";
				}
					
	
		}
?>
<form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" enctype="multipart/form-data">
                                    <div class="col-sm-12 text-white">
                                        <br><label><span class="fa fa-book"></span> Book Name  </label>
                                                                <div class="col-sm-10 col-md-10"><input type="text" name="bname" required class="form-control"></div>
                                                            </div>
                                                            <div class="col-sm-12 text-white">
                                         <br><label><span class="fa fa-users"></span> Author Name  </label>
                                                                <div class="col-sm-10 col-md-10"><input type="text" name="author" required class="form-control"></div>
                                                            </div>
                                                            <div class="col-sm-12 text-white"><br><label><span class="fa fa-tags"></span> Category </label>
                                                            <div class="col-sm-10 col-md-10">
                                                                <select name="category" required class="form-control">
                                                                    <option value="">Select</option>
                                                                    <option value="Engineering">Engineering</option>
                                                                    <option value="Medical"> Medical</option>
                                                                    <option value="General">General</option>
                                                                    <option value="School Books">School Books</option>
                                                                    <option value="Others"> Others</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 text-white">
                                    <br><label><span class="fa fa-key"></span> Keywords  </label>
                                                            <div class="col-sm-10 col-md-10"><textarea name="keys"  style="resize: none;" required class="form-control"></textarea></div>
                                                        </div>
                                                        <div class="col-sm-12 text-white">
                                                            <br><label><span class="fa fa-link"></span> Link  </label>
                                                                <div class="col-sm-10 col-md-10"> <input type="url" name="link"  class="form-control" required ></div>
                                                            </div>

                                 <div class="col-sm-12"><br>
                                    <button type="submit" name="submit" class="btn btn-block btn-info "><span class="fa fa-plus"> </span>  Upload Book</button>
                                </div>
	  </form>
        </div>
        <div class="col-sm-2"></div>
    </div>
</div>








   <nav class="navbar navbar-inverse">
       <center>
          <p class="text-white">Designed by <a href="https://facebook.com/praveenrambalu"> Praveenram Balachandran</a></p> 
       <img src="img/logorect.png" class="img img-responsive " style="max-height:100px; margin-bottom:20px;">

</center>
</nav>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js "></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js "></script>
        <script src="js/main.js "></script>
        <script>
   $("document").ready(function() {
    $("#calert").click(function() {
        alert("We know you are curious to see the topic wise book...!       Sorry for your inconvenient..!       The updation Process is going on We will surely update soon..!");
    });
});
       
        </script>
    </body>

    </html>

    </html>
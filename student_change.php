<?php
	include "database.php";
	include "function.php";
	
	session_start();
if(!isset($_SESSION["ID"]))
{
	header('Location: index.php');

	// echo "<script>window.open('index.php','_self')</script>";
}
?>
    <!DOCTYPE html>
    <html lang="en">
<?php include "head.php";
	include "keysplitter.php";
 ?>
    </head>

    <body>
        <nav class="navbar navbar-default">
            <div class="">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mynavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>

            </button>
                    <a href="index.php" class="navbar-brand text-uppercase" style="color:white;">
                        <img src="img/logorect.png" class="img-responsive">
                    </a>
                </div>
                <div id="mynavbar" class="collapse navbar-collapse text-uppercase">
                    <ul class="nav navbar-nav navbar-right ">
					
                        <li><a href="" class="active">Hi...<?php echo $_SESSION["NAME"]; ?></a></li>
                        <li><a href="student_home.php"><span class="fa fa-home"></span> Home</a></li>
                        <li><form class="navbar-form navbar-left" action="search_book.php">
      <div class="form-group">
        <input type="text" name="name" required class="form-control " placeholder="Search" >
      </div>
      <button type="submit" class="btn btn-default"><span class="fa fa-search"></span></button>
    </form></li> 
                       
                        <!-- <li><a href="search_book.php"><span class="fa fa-search"></span> Search Books</a></li> -->
                        <li><a href="add_req.php"><span class="fa fa-paper-plane"></span> Request</a></li>
                        <li><a href="student_change.php"><span class="fa-passwd-reset fa-stack">
                                                         <i class="fa fa-undo fa-stack-2x"></i>
                                                            <i class="fa fa-lock fa-stack-1x"></i>
                                                                                        </span> Change Password</a></li>
                        <li><a href="logout.php"><span class="fa fa-power-off"></span> Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- body -->


<div class="container">
    <div class="row well outer">
        <div class="col-sm-2"></div>
        <div class="col-sm-8 text-white">
            <h3 class="text-white text-uppercase">Change Your Password</h3>
<?php
if(isset($_POST["submit"]))
		{
			$opass=$_POST["opass"];
			$enopass=base64_encode($opass);
			$sql="SELECT * FROM student WHERE PASS='$enopass' and ID=".$_SESSION["ID"];
			$res=$db->query($sql);
			if($res->num_rows>0)
			{
				$npass=$_POST["npass"];
				$ennpass=base64_encode($npass);
				$s="update student set PASS='$ennpass' WHERE ID=".$_SESSION["ID"];
				$db->query($s);
				echo "<p class='text-white'>Password Changed </p>";
			}
			else
			{
				echo "<p class='text-white'>Invalid Password</p>";
			}

		}
	?>




            <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" autocomplete="off" >
        <div class="col-sm-12"> <br><label><span class="fa fa-history"></span> Old Password  </label>
            <div class="col-sm-10 col-md-10"><input type="password" name="opass" required class="form-control"></div>
        </div>
        <div class="col-sm-12"> <br><label><span class="fa fa-lock"></span> New Password  </label>
            <div class="col-sm-10 col-md-10"><input type="password" name="npass" required class="form-control"></div>
        </div>
        
         <div class="col-sm-12">
             <br>
       
             <div class="col-sm-10 col-md-10">
           <button class="btn btn-block btn-success" type="submit" name="submit">
            <span class="fa-passwd-reset fa-stack">
                                                         <i class="fa fa-undo fa-stack-2x"></i>
                                                            <i class="fa fa-lock fa-stack-1x"></i>
                                                                                        </span> 

            Change Now</button></div>
    </div>
</form>
        </div>
        <div class="col-sm-2"></div>
    </div>
</div>




<?php include "bannerad.php"; ?>

   <nav class="navbar navbar-inverse">
       <center>
          <p class="text-white">Designed by <a href="https://facebook.com/praveenrambalu"> Praveenram Balachandran</a></p> 
       <img src="img/logorect.png" class="img img-responsive " style="max-height:100px; margin-bottom:20px;">

</center>
</nav>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js "></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js "></script>
       
        <script src="js/main.js "></script>
        <script>
   $("document").ready(function() {
            
});
       
        </script>
    </body>

    </html>

    </html>
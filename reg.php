<?php
	include "database.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
   <?php include "head.php"; ?>
</head>

<body>
    <nav class="navbar">
        <div class="">
            <div class="navbar-header">
                <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mynavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>

            </button> -->
                <a href="index.php" class="navbar-brand text-uppercase" style="color:white;">
                    <img src="img/logorect.png" class="img-responsive">
                </a>
            </div>
            <!-- <div id="mynavbar" class="collapse navbar-collapse text-uppercase">
                <ul class="nav navbar-nav "></ul>
            </div> -->
        </div>
    </nav>

    <div class="container">
        <div class="row well none">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <div class="container col-sm-12">
                    <div class="row well outer text-white">
                        <h3 class="text-center logintitle">Create your Free Account</h3>
                        <p class="pull-right">Already have an account ..? <a href="index.php" class="btn btn-login">Login</a></p>

                        <br>

                        <div class="form">
						<?php
	if(isset($_POST["submit"]))
		{
			$name=$_POST["name"];
			$pass=$_POST["pass"];
			$mail=$_POST["mail"];
			$dep=$_POST["dep"];
			$gender=$_POST["gender"];
			$enpass=base64_encode($pass);
		$sql="SELECT * FROM student WHERE MAIL='$mail';";
		$res=$db->query($sql);
		//echo $res->num_rows;
			if($res->num_rows>0)
			 {
				echo "<p class='failure'>Sorry Your Mail Already Exists. Please <a href='index.php'>Login here</a></p>";
			 }
			else{
				$sql="INSERT INTO student(NAME,GENDER,PASS,MAIL,DEP)
					VALUES ('{$name}','{$gender}','{$enpass}','{$mail}','{$dep}')";
					
				if($db->query($sql))
				{
				echo "<p class='success'>User Registration Success.</p>";
				$enmail=base64_encode($mail);
							$to=$mail; // Receiver Email ID, Replace with your email ID
			            	$subject='Validate your mail id by clicking this link';
							$message="\n"."Please click the following http://estudylib.tk/confirm_mail.php?confirm=".$enmail."\n if link not working please copy paste to your browser \n Thank you";
							$header="From: estudylib@gmail.com";
							$retval = mail($to,$subject,$message,$header);
							if($retval == true){
								echo "<p class='text-success'>Mail Validate key sent to your mail id".", Thanks for using E-Study-Lib services! if mail is not in the inbox check spam folder </p>";
							}
				}
				else
				{
				echo "<p class='failure'>Registration Failed.</p>";
				}
			}
}
?>
						
						
                            <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" autocomplete="off" id="register">
                                <div class="col-sm-12"> <br><label><span class="fa fa-user"></span> Name  </label>
                                    <div class="col-sm-10 col-md-10"><input type="text" name="name" required class="form-control"></div>
                                </div>
                                <div class="col-sm-12"><br><label><span class="fa fa-venus-mars"></span> Gender </label>
                                    <div class="col-sm-10 col-md-10">
                                        <select name="gender" required class="form-control">
                                            <option value="">Select</option>
                                         <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-12"><br><label><span class="fa fa-building"></span> Department </label>
                                    <div class="col-sm-10 col-md-10">
                                        <select name="dep" required class="form-control">
			                                                    <option value="">Select</option>
			                                                    <option value="CSE">CSE</option>
		                                                    	<option value="Bio Medical">Bio Medical</option>
		                                                    	<option value="ECE">ECE</option>
		                                                    	<option value="EEE">EEE</option>
		                                                    	<option value="MECH">MECHANICAL</option>
		                                                    	<option value="CIVIL">CIVIL</option>
		                                                    	<option value="AUTOMOBILE">AUTOMOBILE</option>
		                                                    	<option value="IT">IT</option>
		                                                    	<option value="others"> Others</option>
		                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-12"><br><label><span class="fa fa-envelope"></span> Email  </label>
                                    <div class="col-sm-10 col-md-10"><input  type="email" name="mail" required  class="form-control"></div>
                                </div>
                                <div class="col-sm-12"><br><label><span class="fa fa-lock"></span> Password  </label>
                                    <div class="col-sm-10 col-md-10"><input type="password"  name="pass" id="pass" required class="form-control"></div>
                                </div>
                                <div class="col-sm-12"><br><label><span class="fa fa-lock"></span> Confirm  </label>
                                    <div class="col-sm-10 col-md-10"><input type="password" name="cpass" class="form-control"></div>
                                </div>

                                <div class="col-sm-12"><br>
                                    <button type="submit" name="submit" class="btn btn-block btn-info ">Register Now</button>
                                </div>
                            </form>

                        </div>

                    </div>
                </div>
            </div>
            <div class="col-sm-2"></div>

        </div>

    </div>




    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js "></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js "></script>
     <script src="js/jquery.validate.min.js"></script>
    <script src="js/main.js "></script>
</body>

</html>

</html>
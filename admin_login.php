<?php
	include "database.php";
	session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
<?php include "head.php"; ?>
</head>

<body>
    <nav class="navbar">
        <div class="">
            <div class="navbar-header">
                <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mynavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>

            </button> -->
                <a href="index.php" class="navbar-brand text-uppercase" style="color:white;">
                    <img src="img/logorect.png" class="img-responsive">
                </a>
            </div>
            <!-- <div id="mynavbar" class="collapse navbar-collapse text-uppercase">
                <ul class="nav navbar-nav ">


                </ul>
            </div> -->
        </div>
    </nav>
    <!-- body -->

    <div class="container">
        <div class="row well none">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <div class="container col-sm-12">
                    <div class="row well outer text-white">
                        <h3 class="text-center text-uppercase logintitle">ADMIN lOGIN</h3>
                        <br>
						
                        <div class="form">
						<?php
	if(isset($_POST["submit"]))
		{
			$sql="SELECT * FROM admin WHERE ANAME='{$_POST["aname"]}' AND APASS='{$_POST["apass"]}'";
			$res=$db->query($sql);
			if($res->num_rows>0)
			{
				$row=$res->fetch_assoc(); 
				$_SESSION["AID"]=$row["AID"];
				$_SESSION["ANAME"]=$row["ANAME"];
				echo "<script>window.open('admin_home.php','_self')</script>";
			}
			else
			{
				echo"<p class='error'>Invalid User name or Password</p>";
			}
		}
?>
                            <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" class="form">
                            <a href="student_home.php" class="btn btn-default  pull-right"> <span class="fa fa-id-badge "></span>  Student</a>
                                <label class="labellogin"><span class="fa fa-user loginfa "></span><b> Username</b></label>
                                <br>
                                <input type="text" name="aname" required class="form-control loginform col-sm-8">
                                <span class="fa fa-info-circle infolog" data-toggle="tooltip" data-placement="right" title="User name is Your Name or Email Address...!"></span>

                                <br>
                                <br>

                                <label class="labellogin"> <span class="fa fa-key loginfa "></span> <b> Password</b></label>
                                <br>
                                <input type="password" name="apass" required class="form-control loginform col-sm-8">
                                <span class="fa fa-info-circle infolog" data-toggle="tooltip" data-placement="right" title="Having trouble login or Forget Passowrd Click Forget Password on Below ....!"></span>
                                <br>
                                <br>
                                <input type="submit" class="btn  loginform btn-primary text-uppercase" name="submit" value="Login">
                            </form>

                        </div>
                        <br>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js "></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js "></script>
    <script src="js/main.js "></script>
</body>

</html>
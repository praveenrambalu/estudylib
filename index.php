<?php
	include "database.php";
	session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
<?php include "head.php"; ?>
</head>

<body>
    <nav class="navbar">
        <div class="">
            <div class="navbar-header">
                <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mynavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>

            </button> -->
                <a href="index.php" class="navbar-brand text-uppercase" style="color:white;">
                    <img src="img/logorect.png" class="img-responsive">
                </a>
            </div>
            <!-- <div id="mynavbar" class="collapse navbar-collapse text-uppercase">
                <ul class="nav navbar-nav ">


                </ul>
            </div> -->
        </div>
    </nav>
    <!-- body -->

    <div class="container">
        <div class="row well none">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <div class="container col-sm-12">
                    <div class="row well outer text-white">
                        <h3 class="text-center text-uppercase logintitle">SIGN IN</h3>
                       <a href="admin_home.php" class="btn btn-default  pull-right"> <span class="fa fa-user" style="font-size:16px;"><span class="fa fa-cog" style="font-size:12px; margin-left: -3px;" ></span></span>  Admin</a>
                        <br>
						<?php
						if(isset($_GET["mes"])){
							$msg=$_GET["mes"];
							echo"<br><div class='alert alert-info alert-dismissible fade in'>
                            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            <strong>$msg</strong>
                        </div><br>";
						}
						?>
                        <div class="form">
						<?php
	if(isset($_POST["submit"]))
		{
			$pass=$_POST["pass"];
			$enpass=base64_encode($pass);
			
			$sql="SELECT * FROM student WHERE MAIL='{$_POST["mail"]}' AND PASS='$enpass' AND STATE='ACTIVE'";
			// echo $sql;
			$res=$db->query($sql);
			if($res->num_rows>0)
			{
				$row=$res->fetch_assoc(); 
				$_SESSION["ID"]=$row["ID"];
				$_SESSION["NAME"]=$row["NAME"];

				echo "<script>window.open('student_home.php','_self')</script>";
			}
			else
			{
				echo"<br><div class='alert alert-warning alert-dismissible fade in'>
                            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            <strong>Wrong Username or Password or <a href='verify_account.php'>You must activate your account Click here</a> </strong>
                        </div><br>";
			}
		}
?>
                            <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" class="form">
                                <label class="labellogin"><span class="fa fa-user loginfa "></span><b> Email</b></label>
                                <br>
                                <input type="email" name="mail" required class="form-control loginform col-sm-8">
                                <span class="fa fa-info-circle infolog" data-toggle="tooltip" data-placement="right" title="User name is Your Email Address...!"></span>

                                <br>
                                <br>

                                <label class="labellogin"> <span class="fa fa-key loginfa "></span> <b> Password</b></label>
                                <br>
                                <input type="password" name="pass" required class="form-control loginform col-sm-8">
                                <span class="fa fa-info-circle infolog" data-toggle="tooltip" data-placement="right" title="Having trouble login or Forget Passowrd Click Forget Password on Below ....!"></span>
                                <br>
                                <br>
                                <input type="submit" class="btn  loginform btn-primary text-uppercase" name="submit" value="Login">
                            </form>

                        </div>
                        <br>
                        <a href="forget_pass.php" class="pull-right btn btn-danger">Forget Password..?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row well none">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <div class="row well outer">
                <h4 class="text-white">Don't have an elibrary account</h4>
                <a href="reg.php" class="btn btn-info btn-block">Create New Account</a>
                </div>
            </div>
            <div class=" col-sm-2 "></div>

        </div>
    </div>





    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js "></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js "></script>
    <script src="js/main.js "></script>
</body>

</html>
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();

    // var data = $('.bkey').text();
    //var arr = data.split(' ');

    //$(".bkey").html("<span class='keytext'><i class='fa fa-star keytextbefore'></i> " + arr[0] + "</span>" + "<span class='keytext'><i class='fa fa-star keytextbefore'></i>  " + arr[1] + "</span>" + "<span class='keytext'><i class='fa fa-star keytextbefore'></i> " + arr[2] + "</span>" + "<span class='keytext'><i class='fa fa-star keytextbefore'></i> " + arr[3] + "</span>");


});



$("document").ready(function() {

    $("#register").validate({
        rules: {
            name: "required",
            dob: {
                required: true,
                date: true

            },
            email: {
                required: true,
                email: true
            },
            pass: {
                required: true,
                minlength: 5
            },
            cpass: {
                required: true,
                minlength: 5,
                equalTo: "#pass"
            },

        },
        messages: {
            name: "Please enter your name",
            dob: {
                required: "Please Enter your Date of Birth",
                date: "Please fill your date in correct format"
            },
            email: {
                required: "Please Enter your Email",
                email: "Please Enter your Email in Correct Format"
            },
            pass: {
                required: "Please Enter your Password",
                minlength: "Password Must 5 letters"
            },
            cpass: {
                required: "Please Enter your Confirm Password",
                minlength: "Password Must 5 letters",
                equalTo: "Passwords doesn't match"
            }
        }

    });

});
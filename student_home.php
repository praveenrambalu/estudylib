<?php
	include "database.php";
	include "function.php";
	// include "keysplitter.php";
	session_start();
	if(!isset($_SESSION["ID"]))
	{
	header('Location: index.php');

		// echo "<script>window.open('admin_login.php','_self')</script>";
	}
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
      <?php
       include "head.php";
       include "keysplitter.php";
       ?>
    </head>

    <body>
        <nav class="navbar navbar-default">
            <div class="">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mynavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>

            </button>
                    <a href="index.php" class="navbar-brand text-uppercase" style="color:white;">
                        <img src="img/logorect.png" class="img-responsive">
                    </a>
                </div>
                <div id="mynavbar" class="collapse navbar-collapse text-uppercase">
                    <ul class="nav navbar-nav navbar-right ">
					
                        <li><a href="" class="active">Hi...<?php echo $_SESSION["NAME"]; ?></a></li>
                        <li><a href="student_home.php"><span class="fa fa-home"></span> Home</a></li>
                       <li><form class="navbar-form navbar-left" action="search_book.php">
      <div class="form-group">
        <input type="text" name="name" required class="form-control " placeholder="Search" >
      </div>
      <button type="submit" class="btn btn-default"><span class="fa fa-search"></span></button>
    </form></li> 
                        
                        <li><a href="add_req.php"><span class="fa fa-paper-plane"></span> Request</a></li>
                        <li><a href="student_change.php"><span class="fa-passwd-reset fa-stack">
                                                         <i class="fa fa-undo fa-stack-2x"></i>
                                                            <i class="fa fa-lock fa-stack-1x"></i>
                                                                                        </span> Change Password</a></li>
                        <li><a href="logout.php"><span class="fa fa-power-off"></span> Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- body -->
        <div class="container">
            <div class="row well">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <p class="">
                        E-Study-Lib is your search engine for your PDF files. As of today we have <b><?php echo countRecord("SELECT * from book",$db); ?></b>  eBooks for you to download for free. No download limits, enjoy it and don't forget to share the love....<i class="fa fa-heart"></i>
                    </p>
                </div>
                <div class="col-sm-2"></div>
            </div>
        </div>
        <div class="container">
            <div class="row well outer">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                <marquee> 
                <b class="lititle">Engineering</b> 
                <b class="lititle">School Books</b>
                <b class="lititle">Devotional</b>
                <b class="lititle">Comics</b> 
                   <b class="lititle">Arts</b>
                   <b class="lititle">nursing</b>
                   <b class="lititle">General</b>
                   <b class="lititle">Economics</b>
                   <b class="lititle">Bio Graphies</b>
                   <b class="lititle">litrals</b>
                  </marquee> 
                </div>
                <div class="col-sm-2"></div>
            </div>
        </div>
        <div class="container">
            <h3 class="text-uppercase text-white">Latest Uploaded Book</h3>
            
			
			<?php 
	
		$sql="SELECT * FROM book ORDER BY BID DESC LIMIT 5";
		$res=$db->query($sql);
		
		if($res->num_rows>0)
		{
			echo '<div class="container">
		<div class="col-sm-8">';
		  
				while($row=$res->fetch_assoc())
				{
					echo "<div class='col-sm-12'>
             <div class='row well wellmoder bcontainer'> 
                    <br>

                    <div class='col-sm-12 col-xs-12'>
                        <h4 class='btitle'>{$row["BTITLE"]}</h4>";
                        $author="{$row["AUTHOR"]}";
                      if ($author!=null) {
                          echo "<p class='bauthor text-uppercase pull-right text-primary'>Author : $author</p>";
                         
                      }
                      
                        echo"
                        <p class='bkeytitle'>Keywords :
						<pre>";
							$key=$row["KEYWORDS"];
							$spkey=keysplitter($key);
							$ikey=0;
							
							while($spkey)
							{
								echo "<span class='fa fa-star keytext'></span>    ";
								echo $spkey[$ikey];
								$ikey++;
								if($ikey==5)
								{
									break;
								}
							}
						echo "</pre>";

                            echo"</i></p>
                              <div class='col-sm-12'>
                        <a href='view_book.php?bid={$row["BID"]}' class='btn'><span class='fa fa-eye'></span> View</a>
                        <a href='{$row["FILE"]}' class='btn'><span class='fa fa-download'></span> Download</a>
                        </div>
                    </div>
                  
                    <br>
				 </div>
				 
			</div>";
					
					
				}
			echo '</div>
        <div class="col-sm-4  categories " id="calert">
        <h3 class="text-uppercase categorieshead"> Categories </h3>
            <div class="row ">
                 <div class="col-sm-6 ">
                        <a href="#"><img src="img/cate.jpg" class="img img-responsive img-circle category"></a>
                        <p class="catetitle" >Engineering</p>
                        </div>
                  <div class="col-sm-6">
                        <a href="#"><img src="img/cate.jpg" class="img img-responsive img-circle category"></a>
                       <p class="catetitle" >School</p>
                        </div>
             </div>
              <div class="row">
                 <div class="col-sm-6 ">
                        <a href="#"><img src="img/cate.jpg" class="img img-responsive img-circle category"></a>
                        <p class="catetitle" >Devotional</p>
                        </div>
                  <div class="col-sm-6">
                        <a href="#"><img src="img/cate.jpg" class="img img-responsive img-circle category"></a>
                       <p class="catetitle" >Comics</p>
                        </div>
             </div>
              <div class="row">
                 <div class="col-sm-6">
                        <a href="#"><img src="img/cate.jpg" class="img img-responsive img-circle category"></a>
                        <p class="catetitle" >arts</p>
                        </div>
                  <div class="col-sm-6">
                        <a href=""><img src="img/cate.jpg" class="img img-responsive img-circle category"></a>
                       <p class="catetitle" >Nursing</p>
                        </div>
             </div>
              <div class="row">
                 <div class="col-sm-6 ">
                        <a href="#"><img src="img/cate.jpg" class="img img-responsive img-circle category"></a>
                        <p class="catetitle" >Economics</p>
                        </div>
                  <div class="col-sm-6 ">
                        <a href="#"><img src="img/cate.jpg" class="img img-responsive img-circle category"></a>
                       <p class="catetitle" > biographies</p>
                        </div>
             </div>
              <div class="row">
                 <div class="col-sm-6 ">
                        <a href="#"><img src="img/cate.jpg" class="img img-responsive img-circle category"></a>
                        <p class="catetitle" >literals</p>
                        </div>
                  <div class="col-sm-6 ">
                        <a href="#"><img src="img/cate.jpg" class="img img-responsive img-circle category"></a>
                       <p class="catetitle" >Stories</p>
                        </div>
             </div>
             <br>
              </div>
        </div>';
			
		}
		else
		{
			echo "<p class='text-danger'>No Book Record Found</p>";
		}
	?>
	
    </div>

		
<?php include "bannerad.php"; ?>

   <nav class="navbar navbar-inverse">
       <center>
          <p class="text-white">Designed by <a href="https://facebook.com/praveenrambalu"> Praveenram Balachandran</a></p> 
       <img src="img/logorect.png" class="img img-responsive " style="max-height:100px; margin-bottom:20px;">

</center>
</nav>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js "></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js "></script>
        <script src="js/main.js "></script>
        <script>
   $("document").ready(function() {
    $("#calert").click(function() {
        alert("We know you are curious to see the topic wise book...!       Sorry for your inconvenient..!       The updation Process is going on We will surely update soon..!");
    });
});
       
        </script>
		
		<div style='text-align: right;position: fixed;z-index:9999999;bottom: 0; width: 100%;cursor: pointer;line-height: 0;display:block !important;'><a title="000webhost logo" rel="nofollow" target="_blank" href="https://www.000webhost.com/free-website-sign-up?utm_source=000webhostapp&amp;utm_campaign=000_logo&amp;utm_campaign=ss-footer_logo&amp;utm_medium=000_logo&amp;utm_content=website"><img src="https://cdn.rawgit.com/000webhost/logo/e9bd13f7/footer-powered-by-000webhost-white2.png" alt="000webhost logo"></a></div>
    </body>
<style>
[rel*="nofollow"] {
    display:none !important;
}
</style>
    </html>

    </html>
<?php
	include "database.php";
	include "function.php";
	
	session_start();
if(!isset($_SESSION["ID"]))
{
	header('Location: index.php');

	// echo "<script>window.open('index.php','_self')</script>";
}
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
     <?php include "head.php";
	include "keysplitter.php";
     
     ?>
    </head>

    <body>
        <nav class="navbar navbar-default">
            <div class="">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mynavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>

            </button>
                    <a href="index.php" class="navbar-brand text-uppercase" style="color:white;">
                        <img src="img/logorect.png" class="img-responsive">
                    </a>
                </div>
                <div id="mynavbar" class="collapse navbar-collapse text-uppercase">
                    <ul class="nav navbar-nav navbar-right ">

                        <li><a href="" class="active">Hi...<?php echo $_SESSION["NAME"]; ?></a></li>
                        <li><a href="student_home.php"><span class="fa fa-home"></span> Home</a></li>
                        <!-- <li><a href="search_book.php"><span class="fa fa-search"></span> Search Books</a></li> -->
                        <li><a href="add_req.php"><span class="fa fa-paper-plane"></span> Request</a></li>
                        <li><a href="student_change.php"><span class="fa-passwd-reset fa-stack">
                                                         <i class="fa fa-undo fa-stack-2x"></i>
                                                            <i class="fa fa-lock fa-stack-1x"></i>
                                                                                        </span> Change Password</a></li>
                        <li><a href="logout.php"><span class="fa fa-power-off"></span> Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- body -->

 <div class="container">
         <div class="row well outer">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <form action="search_book.php" method="get">
                        <label class="text-uppercase text-white ">Enter Book Name or Keyword</label>
                        <input type="text" name="name" required class="form-control " style="width:80%; display:inline-block;">
                        <button type="submit"  class="btn  btn-info "><span class="fa fa-search"></span></button>
                    </form>

                    <br>
                    <button class="btn btn-info " id="catebtn"><span class="fa fa-tags"> </span>   Search By Category</button>
                    <div class="form" id="catediv">
                        <form action="search_book.php" required method="get">

                            <select name="category" class="form-control" style="width:80%; display:inline;">
            <option value="">ALL</option>
            <option value="Engineering">Engineering</option>
            <option value="Medical"> Medical</option>
            <option value="General">General</option>
            <option value="School Books">School Books</option>
            <option value="Others"> Others</option>
        </select>

                            <button type="submit"  class="btn btn-default"><span class="fa fa-search"></span></button>
                        </form>
                    </div>



                </div>
                <div class="col-sm-2"></div>
            </div>
            
            <div class="row well outer">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <?php
								if(isset($_GET["name"]))
								{
									$name=$_GET["name"];

								}
								?>
								<?php
								if(isset($_GET["category"]))
								{
									$category=$_GET["category"];

								}
								?>
													<?php
							  if (isset($_GET["pg"])) {
								 $limit=$_GET["pg"];
								
							  }
							  else{
								  $limit=5;
							  }
							  ?>
                            <input type="hidden" value="<?php echo $limit; ?>" id="value">
                            <input type="hidden" value="<?php echo $name;?>" id="name">

<?php 
        if(isset($_GET["name"]))
		{
           
        $sql="SELECT * FROM book WHERE BTITLE like '%{$_GET["name"]}%' or keywords like '%{$_GET["name"]}%' LIMIT $limit";
          
			$res=$db->query($sql);
			if($res->num_rows>0)
		{
			echo '<div class="container">
        <div class="col-sm-8">';
     
				while($row=$res->fetch_assoc())
				{
                   
              	echo "<div class='col-sm-12'>
             <div class='row well wellmoder bcontainer'> 
                    <br>

                    <div class='col-sm-12 col-xs-12'>
                        <h4 class='btitle'>{$row["BTITLE"]}</h4>";
                        $author="{$row["AUTHOR"]}";
                      if ($author!=null) {
                          echo "<p class='bauthor text-uppercase pull-right text-black'>Author : $author</p>";
                         
                      } echo"
                        <p class='bkeytitle'>Keywords :
						<pre>";
							$key=$row["KEYWORDS"];
							$spkey=keysplitter($key);
							$ikey=0;
							
							while($spkey)
							{
								echo "<span class='fa fa-star keytext'></span>    ";
								echo $spkey[$ikey];
								$ikey++;
								if($ikey==5)
								{
									break;
								}
							}
						echo "</pre>";

                            echo"</i></p>
                              <div class='col-sm-12'>
                        <a href='view_book.php?bid={$row["BID"]}' class='btn'><span class='fa fa-eye'></span> View</a>
                        <a href='{$row["FILE"]}' class='btn'><span class='fa fa-download'></span> Download</a>
                        </div>
                    </div>
                  
                    <br>
				 </div>
				 
			</div>";
					
					
                }
               echo '  <center><button id="loadmore" class="btn btn-default "><span class="fa fa-refresh"></span>  Load More</button> </center>';
			echo '</div> ';
       
			
		}
			else
			{
                echo "<p class='text-white'>
                Very Sorry My dear Friend..! <br><br>
                We don't have the Book which you have searched..! 
                <br><br>
                If you Really Need the Book Urgently Please <a href='add_req.php' class='btn btn-req' >Request here </a> 
                
                </p>";
			}
        }
        if(isset($_GET["category"]))
		{
           
			$sql="SELECT * FROM book WHERE CATEGORY like '%{$_GET["category"]}%' or CATEGORY like '%{$_GET["category"]}%' LIMIT $limit ";
        
          
			$res=$db->query($sql);
			if($res->num_rows>0)
		{
			echo '<div class="container">
        <div class="col-sm-8">';
     
				while($row=$res->fetch_assoc())
				{
                   
              	echo "<div class='col-sm-12'>
             <div class='row well wellmoder bcontainer'> 
                    <br>

                    <div class='col-sm-12 col-xs-12'>
                        <h4 class='btitle'>{$row["BTITLE"]}</h4>";
                        $author="{$row["AUTHOR"]}";
                      if ($author!=null) {
                          echo "<p class='bauthor text-uppercase pull-right text-black'>Author : $author</p>";
                         
                      }
                      
                        echo"
                        <p class='bkeytitle'>Keywords :
						<pre>";
							$key=$row["KEYWORDS"];
							$spkey=keysplitter($key);
							$ikey=0;
							
							while($spkey)
							{
								echo "<span class='fa fa-star keytext'></span>    ";
								echo $spkey[$ikey];
								$ikey++;
								if($ikey==5)
								{
									break;
								}
							}
						echo "</pre>";

                            echo"</i></p>
                              <div class='col-sm-12'>
                        <a href='view_book.php?bid={$row["BID"]}' class='btn'><span class='fa fa-eye'></span> View</a>
                        <a href='{$row["FILE"]}' class='btn'><span class='fa fa-download'></span> Download</a>
                        </div>
                    </div>
                  
                    <br>
				 </div>
				 
				 
			</div>";
					
					
                }
               echo '  <center><button id="loadmore" class="btn btn-default "><span class="fa fa-refresh"></span>  Load More</button> </center>';
			echo '</div>';
			echo '</div>';
       
			
		}
			else
			{
                echo "<p class='text-white'>
                Very Sorry My dear Friend..! <br><br>
                We don't have the Book which you have searched..! 
                <br><br>
                If you Really Need the Book Urgently Please <a href='add_req.php' class='btn btn-req' >Request here </a> 
                
                </p>";
			}
		}
    
    ?>

                </div>
                </div>
                <div class="col-sm-2"></div>
            </div>
            

            
        
        
</div>

        <?php
  if (isset($_GET["pg"])) {
    
     echo '<script>
     $("html, body").animate({ scrollTop: $(document).height() }, 10000);
     </script>';
  }
 
  ?>
<?php include "bannerad.php"; ?>


            <nav class="navbar navbar-inverse">
                <center>
                    <p class="text-white">Designed by <a href="https://facebook.com/praveenrambalu"> Praveenram Balachandran</a></p>
                    <img src="img/logorect.png" class="img img-responsive " style="max-height:100px; margin-bottom:20px;">

                </center>
            </nav>

            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js "></script>
            <script src="js/main.js "></script>
            <script>
                $("document").ready(function() {

                    $("#catediv,#catebtn").hide();
                    $("#catebtn").click(function() {
                       

                    });


                    $("#loadmore").click(function() {
                        var value = $("#value").val();
                        var name = $("#name").val();
                        var value2 = 5;
                      
                        var newval = parseInt(value) + parseInt(value2);
                        $("#loadingvalue").val(newval);
                        $valuee = "search_book.php?name="+name+"&pg=" + newval;
                        window.open($valuee, '_self')
                    });

                });
                //    window.open('index.php','_self')
            </script>
       
    </body>

    </html>
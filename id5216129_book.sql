-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 13, 2018 at 01:47 PM
-- Server version: 10.2.12-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id5216129_book`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `AID` int(11) NOT NULL,
  `ANAME` varchar(150) NOT NULL,
  `APASS` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`AID`, `ANAME`, `APASS`) VALUES
(1, 'praveenram', 'ramjibaba'),
(2, 'admin', '@dminestudylib.tk');

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `BID` int(11) NOT NULL,
  `BTITLE` varchar(1500) DEFAULT NULL,
  `KEYWORDS` varchar(1500) DEFAULT NULL,
  `AUTHOR` varchar(50) DEFAULT NULL,
  `CATEGORY` varchar(50) DEFAULT NULL,
  `FILE` varchar(150) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`BID`, `BTITLE`, `KEYWORDS`, `AUTHOR`, `CATEGORY`, `FILE`) VALUES
(1, 'Artificial intelligence Question Bank', 'Artificial intelligence Question Bank qb ', NULL, NULL, 'https://drive.google.com/open?id=1r9uEJVTzbfXm9JZhvaMxjJRjYfR368D9'),
(2, 'Mobile Computing Notes ', 'Mobile Computing Notes MC QB Unit wise', NULL, NULL, 'https://drive.google.com/open?id=1IB904zLik_sgXnyA9ufL8t8ImxfJT1zB'),
(3, 'CS 6659 Artificial intelligence Question Paper May/June 2016', 'CS 6659 Artificial intelligence Question Paper may june 2016 cs6659', NULL, NULL, 'https://drive.google.com/open?id=1cQYz-jxGDrPsCu9aos5QlA5CR_A-yDXN'),
(4, 'CS 6659 Artificial intelligence Unit 3 notes', 'Cs 6659 Artificial intelligence Unit 3 unit wise notes cs6659', NULL, NULL, 'https://drive.google.com/open?id=1IeqqyNODvX7lyf1oiw4wtppq-bOEs8Op'),
(5, 'CS 6659 Artificial intelligence Unit 4 notes', 'Cs 6659 artificial intelligence unit 4 notes iv notes questions wise cs6659', NULL, NULL, 'https://drive.google.com/open?id=128DKZa7H371o4gG0WMGP-lnEwgPh0MJq'),
(6, 'CE 6602 Structural Analysis-2 QUESTION bank', 'ce6602 Structural Analysis-2 QUESTION bank qb civil', NULL, NULL, 'https://drive.google.com/open?id=1aHqRBzoa4IL7x01XgVtTSiSRO1aoIzu2'),
(7, 'CS 6659 Artificial intelligence Question Paper November/December 2016', 'CS 6659 Artificial intelligence Question Paper November/December 2016\r\nQB  cs6659', NULL, NULL, 'https://drive.google.com/open?id=19LUYF6b-KceJy-7WET3Umlmcmoz2gnDb'),
(8, 'Cs 6601/IT 6601 Mobile Computing Material', 'Cs6601/IT6601 Mobile Computing Material study notes unit wise ', NULL, NULL, 'https://drive.google.com/open?id=197O893cfCe7JBcyByu_0RVMrXE69KJDA'),
(9, 'HS8151 Notes 1', 'Communicative english 2017 regulation english notes ', NULL, NULL, 'http://www.mediafire.com/download/r526y0c6lq7gyp9/HS6151_-_Technical_English_I_-_Notes_&_Question_Bank.pdf'),
(10, 'HS8151 Notes 2 ', 'Communicative english 2017 regulation english notes ', NULL, NULL, 'http://www.mediafire.com/download/xo8tu2axfaezqxv/hs6151.pdf'),
(11, 'HS8151 Notes 3', 'Communicative english 2017 regulation english notes ', NULL, NULL, 'http://www.mediafire.com/download/g6fewqsd11ygyd8/HS6151_qb.pdf'),
(12, 'HS8151 Question bank', 'Communicative english 2017 regulation english notes  qb', NULL, NULL, 'http://www.mediafire.com/file/36bqdle1g7k2pfj/HS8151-Communicative_English.pdf'),
(13, 'CY8151 Notes', 'cy8151 engineering chemistry 2017 regulation notes qb', NULL, NULL, 'http://www.mediafire.com/download/04y14amahwuncl8/CY6151-CHEM-PART-B-with-hint.pdf'),
(14, 'CY8151 Question bank link 1 download', 'CY8151 Question bank link 1 download engineering chemistry 1', NULL, NULL, 'http://www.mediafire.com/file/x7yn8yz2fsdigtv/CY8151-Engineering_Chemistry.pdf'),
(15, 'CY8151 Question bank link 2 download', 'CY8151 Question bank  download engineering chemistry 1', NULL, NULL, 'http://www.mediafire.com/download/xsn9syns8amtpa8/CY6151-Engineering_Chemistry-I.pdf'),
(16, 'CY8151 Part A (2 marks) with answers', 'CY8151 Part A (2 marks) with answers  engineering chemistry 1', NULL, NULL, 'http://www.mediafire.com/download/0x59sn7t7ckktwt/CY6151-Chemsitry-Part-A.pdf'),
(17, 'CY8151 Part B with Hints material download', 'CY8151 Engineering Chemistry Anna university 1st semester Regulation 2017 notes\r\n\r\n \r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nCY8151 Engineering Chemistry Notes Syllabus\r\nAnna University CY8151 Engineering Chemistry 1st semester notes\r\nCY8151 Engineering Chemistry Notes\r\nCY8151 Engineering Chemistry Syllabus \r\nCY8151 Engineering Chemistry Question papers\r\nCY8151 Engineering Chemistry Question papers\r\nCY8151 Engineering Chemistry Question Bank\r\nAnna University 1st Semester notes\r\nBE 1st Semester Anna University Notes', NULL, NULL, 'http://www.mediafire.com/download/04y14amahwuncl8/CY6151-CHEM-PART-B-with-hint.pdf'),
(18, 'PH8151 Notes 1 link ', 'PH8151 Engineering Physics Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nPH8151 Engineering Physics Notes Syllabus\r\nAnna University PH8151 Engineering Physics 1st semester notes\r\nPH8151 Engineering Physics Notes', NULL, NULL, 'http://www.mediafire.com/download/682yh1ajf7p1p2e/PH6151-Engineering_Physics_-_I.pdf'),
(19, 'PH8151 Notes 2 ', 'PH8151 Engineering Physics Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nPH8151 Engineering Physics Notes Syllabus\r\nAnna University PH8151 Engineering Physics 1st semester notes', NULL, NULL, 'http://www.mediafire.com/download/h61c1n8b0oa8g0d/PH6151-Physics-part-B-with-hint.pdf'),
(20, 'PH8151 Question Bank', 'PH8151 Engineering Physics Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nPH8151 Engineering Physics Notes Syllabus\r\nAnna University PH8151 Engineering Physics 1st semester notes\r\nPH8151 Engineering Physics Notes\r\nPH8151 Engineering Physics Syllabus \r\nPH8151 Engineering Physics Question papers\r\nPH8151 Engineering Physics Question papers\r\nPH8151 Engineering Physics Question Bank', NULL, NULL, 'http://www.mediafire.com/file/xi9mzad8s062c4a/PH8151-Engineering_Physics.pdf'),
(21, 'PH8151 Part B with Hints material', 'PH8151 Engineering Physics Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nPH8151 Engineering Physics Notes Syllabus\r\nAnna University PH8151 Engineering Physics 1st semester notes\r\nPH8151 Engineering Physics Notes\r\nPH8151 Engineering Physics Syllabus \r\nPH8151 Engineering Physics Question papers\r\nPH8151 Engineering Physics Question papers\r\nPH8151 Engineering Physics Question Bank\r\nAnna University 1st Semester notes', NULL, NULL, 'http://www.mediafire.com/download/h61c1n8b0oa8g0d/PH6151-Physics-part-B-with-hint.pdf'),
(22, 'PH8151 Previous year Question paper ', 'PH8151 Engineering Physics Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nPH8151 Engineering Physics Notes Syllabus\r\nAnna University PH8151 Engineering Physics 1st semester notes\r\nPH8151 Engineering Physics Notes\r\nPH8151 Engineering Physics Syllabus \r\nPH8151 Engineering Physics Question papers\r\nPH8151 Engineering Physics Question papers', NULL, NULL, 'http://www.rejinpaul.com/2014/06/ph6151-engineering-physics-i-question-papers-previous-year-regulation-2013-1st-semester.html'),
(23, 'MA8151 Notes 1', 'MA8151 Engineering Mathematics I Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nMA8151 Engineering Mathematics 1 Notes Syllabus\r\nAnna University MA8151 Engineering Mathematics I 1st semester notes\r\nMA8151 Engineering Mathematics I Notes\r\nMA8151 Engineering Mathematics I Syllabus \r\nMA8151 Engineering Mathematics I Question papers\r\nMA8151 Engineering Mathematics I Question papers\r\nMA8151 Engineering Mathematics I Question Bank\r\nAnna University 1st Semester notes', NULL, NULL, 'http://www.mediafire.com/download/sd88ysy05tcibbd/MA6151_-_Mathematics_-_I_notes.pdf'),
(24, 'MA8151 Notes 2', 'MA8151 Engineering Mathematics I Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nMA8151 Engineering Mathematics 1 Notes Syllabus\r\nAnna University MA8151 Engineering Mathematics I 1st semester notes\r\nMA8151 Engineering Mathematics I Notes\r\nMA8151 Engineering Mathematics I Syllabus', NULL, NULL, 'http://www.mediafire.com/download/ujlqxdpgdvt55kk/ma6151_formula.pdf'),
(25, 'MA8151 Formula link ', 'MA8151 Engineering Mathematics I Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nMA8151 Engineering Mathematics 1 Notes Syllabus\r\nAnna University MA8151 Engineering Mathematics I 1st semester notes\r\nMA8151 Engineering Mathematics I Notes\r\nMA8151 Engineering Mathematics I Syllabus \r\nMA8151 Engineering Mathematics I Question papers', NULL, NULL, 'http://www.mediafire.com/download/ujlqxdpgdvt55kk/ma6151_formula.pdf'),
(26, 'MA8151 Question bank link 1', 'MA8151 Engineering Mathematics I Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nMA8151 Engineering Mathematics 1 Notes Syllabus\r\nAnna University MA8151 Engineering Mathematics I 1st semester notes\r\nMA8151 Engineering Mathematics I Notes\r\nMA8151 Engineering Mathematics I Syllabus \r\nMA8151 Engineering Mathematics I Question papers\r\nMA8151 Engineering Mathematics I Question papers\r\nMA8151 Engineering Mathematics I Question Bank', NULL, NULL, 'http://www.mediafire.com/download/zzlka87qpbt8y8b/MA6151-Mathematics-I.pdf'),
(27, 'MA8151 Question bank link 2(R2013)', 'MA8151 Engineering Mathematics I Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nMA8151 Engineering Mathematics 1 Notes Syllabus\r\nAnna University MA8151 Engineering Mathematics I 1st semester notes\r\nMA8151 Engineering Mathematics I Notes\r\nMA8151 Engineering Mathematics I Syllabus \r\nMA8151 Engineering Mathematics I Question papers\r\nMA8151 Engineering Mathematics I Question papers\r\nMA8151 Engineering Mathematics I Question Bank\r\nAnna University 1st Semester notes\r\nBE 1st Semester Anna University Notes', NULL, NULL, 'http://www.mediafire.com/download/191q9luaszduy6z/ma6151_qb.pdf'),
(28, 'MA8151 Question bank link 2(R2013)', 'MA8151 Engineering Mathematics I Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nMA8151 Engineering Mathematics 1 Notes Syllabus\r\nAnna University MA8151 Engineering Mathematics I 1st semester notes\r\nMA8151 Engineering Mathematics I Notes\r\nMA8151 Engineering Mathematics I Syllabus \r\nMA8151 Engineering Mathematics I Question papers\r\nMA8151 Engineering Mathematics I Question papers', NULL, NULL, 'http://www.mediafire.com/download/f7wb8oy39j4goch/ma6151_qb2.pdf'),
(29, 'MA8151 Question bank link 3 (R2013)', 'MA8151 Engineering Mathematics I Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nMA8151 Engineering Mathematics 1 Notes Syllabus\r\nAnna University MA8151 Engineering Mathematics I 1st semester notes\r\nMA8151 Engineering Mathematics I Notes\r\nMA8151 Engineering Mathematics I Syllabus \r\nMA8151 Engineering Mathematics I Question papers\r\nMA8151 Engineering Mathematics I Question papers\r\nMA8151 Engineering Mathematics I Question Bank', NULL, NULL, 'http://www.mediafire.com/download/f7wb8oy39j4goch/ma6151_qb2.pdf'),
(30, 'MA8151 Question bank link 4', 'MA8151 Engineering Mathematics I Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nMA8151 Engineering Mathematics 1 Notes Syllabus\r\nAnna University MA8151 Engineering Mathematics I 1st semester notes\r\nMA8151 Engineering Mathematics I Notes\r\nMA8151 Engineering Mathematics I Syllabus \r\nMA8151 Engineering Mathematics I Question papers', NULL, NULL, 'http://www.mediafire.com/file/bf2zlf4g4s3s4r0/MA8151-Engineering_Mathematics-I.pdf'),
(31, 'MA8151 Previous year Question paper', 'MA8151 Engineering Mathematics I Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nMA8151 Engineering Mathematics 1 Notes Syllabus\r\nAnna University MA8151 Engineering Mathematics I 1st semester notes\r\nMA8151 Engineering Mathematics I Notes\r\nMA8151 Engineering Mathematics I Syllabus \r\nMA8151 Engineering Mathematics I Question papers\r\nMA8151 Engineering Mathematics I Question papers\r\nMA8151 Engineering Mathematics I Question Bank', NULL, NULL, 'http://www.rejinpaul.com/2014/06/hs6151-technical-english-i-question-papers-regulation-2013-1st-semester.html'),
(32, 'GE8151 Notes 1 ', 'GE8151 Problem solving and Python Programming Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nGE8151 Problem solving and Python Programming Notes Syllabus\r\nAnna University GE8151 Problem solving and Python Programming 1st semester notes\r\nGE8151 Problem solving and Python Programming Notes\r\nGE8151 Problem solving and Python Programming Syllabus \r\nGE8151 Problem solving and Python Programming Question papers\r\nGE8151 PSPP Question papers', NULL, NULL, 'http://www.mediafire.com/file/ta3rv29vzauxl9x/GE8151_notes.pdf'),
(33, 'GE8151 Notes 2', 'GE8151 Problem solving and Python Programming Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nGE8151 Problem solving and Python Programming Notes Syllabus\r\nAnna University GE8151 Problem solving and Python Programming 1st semester notes\r\nGE8151 Problem solving and Python Programming Notes\r\nGE8151 Problem solving and Python Programming Syllabus \r\nGE8151 Problem solving and Python Programming Question papers\r\nGE8151 PSPP Question papers', NULL, NULL, 'http://www.mediafire.com/file/20e7j0dphlx6j82/GE8151_Rejinpaul_notes_Python.pdf'),
(34, 'GE8151 Question Bank', 'GE8151 Problem solving and Python Programming Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nGE8151 Problem solving and Python Programming Notes Syllabus\r\nAnna University GE8151 Problem solving and Python Programming 1st semester notes\r\nGE8151 Problem solving and Python Programming Notes\r\nGE8151 Problem solving and Python Programming Syllabus \r\nGE8151 Problem solving and Python Programming Question papers\r\nGE8151 PSPP Question papers\r\nGE8151 Problem solving and Python Programming Question Bank\r\nAnna University 1st Semester notes\r\nBE 1st Semester Anna University Notes', NULL, NULL, 'http://www.mediafire.com/file/dmzvwh7gsf12dth/GE8151_PSPPP_rejinpaul_QB.pdf'),
(35, 'GE8151 Question Bank', 'GE8151 Problem solving and Python Programming Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nGE8151 Problem solving and Python Programming Notes Syllabus\r\nAnna University GE8151 Problem solving and Python Programming 1st semester notes\r\nGE8151 Problem solving and Python Programming Notes\r\nGE8151 Problem solving and Python Programming Syllabus \r\nGE8151 Problem solving and Python Programming Question papers\r\nGE8151 PSPP Question papers\r\nGE8151 Problem solving and Python Programming Question Bank\r\nAnna University 1st Semester notes\r\nBE 1st Semester Anna University Notes', NULL, NULL, 'http://www.mediafire.com/file/dmzvwh7gsf12dth/GE8151_PSPPP_rejinpaul_QB.pdf'),
(36, 'GE8151 Question Bank-1', 'GE8151 Problem solving and Python Programming Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nGE8151 Problem solving and Python Programming Notes Syllabus\r\nAnna University GE8151 Problem solving and Python Programming 1st semester notes\r\nGE8151 Problem solving and Python Programming Notes\r\nGE8151 Problem solving and Python Programming Syllabus \r\nGE8151 Problem solving and Python Programming Question papers\r\nGE8151 PSPP Question papers', NULL, NULL, 'http://www.mediafire.com/file/4uhb69lbzo484jb/GE8151-Problem_Solving_and_Python_Programming.pdf'),
(37, 'GE8151 Question Bank-2', 'GE8151 Problem solving and Python Programming Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nGE8151 Problem solving and Python Programming Notes Syllabus\r\nAnna University GE8151 Problem solving and Python Programming 1st semester notes\r\nGE8151 Problem solving and Python Programming Notes\r\nGE8151 Problem solving and Python Programming Syllabus \r\nGE8151 Problem solving and Python Programming Question papers\r\nGE8151 PSPP Question papers', NULL, NULL, 'http://www.mediafire.com/file/4bcywpv9dnvuv0j/ge8151-python-programming-question-bank.pdf'),
(38, 'GE8151 Two Marks with Answers', 'GE8151 Problem solving and Python Programming Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nGE8151 Problem solving and Python Programming Notes Syllabus\r\nAnna University GE8151 Problem solving and Python Programming 1st semester notes\r\nGE8151 Problem solving and Python Programming Notes\r\nGE8151 Problem solving and Python Programming Syllabus \r\nGE8151 Problem solving and Python Programming Question papers\r\nGE8151 PSPP Question papers\r\nGE8151 Problem solving and Python Programming Question Bank', NULL, NULL, 'http://www.mediafire.com/file/vdm0vvezlbf4ygs/GE8151_2marks_Rejinpaul.pdf'),
(39, 'GE8152 Notes 1', 'GE8152 Engineering Graphics Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nGE8152 Engineering Graphics Notes Syllabus\r\nAnna University GE8152 Engineering Graphics 1st semester notes\r\nGE8152 Engineering Graphics Notes\r\nGE8152 Engineering Graphics Syllabus \r\nGE8152 Engineering Graphics Question papers\r\nGE8152 EG Question papers', NULL, NULL, 'http://www.mediafire.com/download/pc9cob2a28l9l8o/eg_notes.pdf'),
(40, 'GE8152 Notes 2', 'GE8152 Engineering Graphics Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nGE8152 Engineering Graphics Notes Syllabus\r\nAnna University GE8152 Engineering Graphics 1st semester notes\r\nGE8152 Engineering Graphics Notes\r\nGE8152 Engineering Graphics Syllabus \r\nGE8152 Engineering Graphics Question papers\r\nGE8152 EG Question papers\r\nGE8152 Engineering Graphics Question Bank\r\nAnna University 1st Semester notes\r\nBE 1st Semester Anna University Notes', NULL, NULL, 'http://www.mediafire.com/download/pc9cob2a28l9l8o/eg_notes.pdf'),
(41, 'GE8152 Question bank link 1', 'GE8152 Engineering Graphics Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nGE8152 Engineering Graphics Notes Syllabus\r\nAnna University GE8152 Engineering Graphics 1st semester notes\r\nGE8152 Engineering Graphics Notes\r\nGE8152 Engineering Graphics Syllabus \r\nGE8152 Engineering Graphics Question papers\r\nGE8152 EG Question papers', NULL, NULL, 'http://www.mediafire.com/file/5uf25gj89cu6em6/GE8152-Engineering_Graphics.pdf'),
(42, 'GE8152 Question bank link (R2013)', 'GE8152 Engineering Graphics Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nGE8152 Engineering Graphics Notes Syllabus\r\nAnna University GE8152 Engineering Graphics 1st semester notes\r\nGE8152 Engineering Graphics Notes\r\nGE8152 Engineering Graphics Syllabus \r\nGE8152 Engineering Graphics Question papers\r\nGE8152 EG Question papers', NULL, NULL, 'http://www.mediafire.com/download/efiwxcfx3tzjzhb/GE6152_qb.pdf'),
(43, 'GE8152 Part B with Hints material ', 'GE8152 Engineering Graphics Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nGE8152 Engineering Graphics Notes Syllabus\r\nAnna University GE8152 Engineering Graphics 1st semester notes\r\nGE8152 Engineering Graphics Notes\r\nGE8152 Engineering Graphics Syllabus \r\nGE8152 Engineering Graphics Question papers\r\nGE8152 EG Question papers', NULL, NULL, 'http://www.mediafire.com/download/sb9aps3eikvdomy/GE-6152-Engineering-Graphics-Part-B.pdf'),
(44, 'GE8152 Previous year Question paper (R2013)', 'GE8152 Engineering Graphics Anna university 1st semester Regulation 2017 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes Syllabus\r\nRegulation 2017 1st Semester Syllabus Notes Anna University\r\nGE8152 Engineering Graphics Notes Syllabus\r\nAnna University GE8152 Engineering Graphics 1st semester notes\r\nGE8152 Engineering Graphics Notes\r\nGE8152 Engineering Graphics Syllabus \r\nGE8152 Engineering Graphics Question papers\r\nGE8152 EG Question papers', NULL, NULL, 'http://www.rejinpaul.com/2014/06/ge6152-engineering-graphics-question-papers-previous-year-eg-regulation-2013-1st-semester.html'),
(45, 'HS6151 Notes 1', 'HS6151 Technical English I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nHS6151 Technical English I Notes\r\nAnna University HS6151 Technical English I 1st semester notes\r\nHS6151 Technical English I Notes\r\nAnna University HS6151 Notes\r\nHS6151 Technical English I Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  HS6151 English 1 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes \r\nAnna University 1st semester Lecture Notes Regulation 2013\r\nAnna University 1st Semester subject notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/r526y0c6lq7gyp9/HS6151_-_Technical_English_I_-_Notes_&_Question_Bank.pdf'),
(46, 'HS6151 Notes 2', 'HS6151 Technical English I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nHS6151 Technical English I Notes\r\nAnna University HS6151 Technical English I 1st semester notes\r\nHS6151 Technical English I Notes\r\nAnna University HS6151 Notes\r\nHS6151 Technical English I Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  HS6151 English 1 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes \r\nAnna University 1st semester Lecture Notes Regulation 2013\r\nAnna University 1st Semester subject notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/xo8tu2axfaezqxv/hs6151.pdf'),
(47, 'HS6151 Notes 3 ', 'HS6151 Technical English I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nHS6151 Technical English I Notes\r\nAnna University HS6151 Technical English I 1st semester notes\r\nHS6151 Technical English I Notes\r\nAnna University HS6151 Notes\r\nHS6151 Technical English I Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  HS6151 English 1 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes \r\nAnna University 1st semester Lecture Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/g6fewqsd11ygyd8/HS6151_qb.pdf'),
(48, 'HS6151 Notes 3', 'HS6151 Technical English I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nHS6151 Technical English I Notes\r\nAnna University HS6151 Technical English I 1st semester notes\r\nHS6151 Technical English I Notes\r\nAnna University HS6151 Notes\r\nHS6151 Technical English I Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  HS6151 English 1 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes \r\nAnna University 1st semester Lecture Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/g6fewqsd11ygyd8/HS6151_qb.pdf'),
(49, 'HS6151 Question bank link 1', 'HS6151 Technical English I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nHS6151 Technical English I Notes\r\nAnna University HS6151 Technical English I 1st semester notes\r\nHS6151 Technical English I Notes\r\nAnna University HS6151 Notes\r\nHS6151 Technical English I Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  HS6151 English 1 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes \r\nAnna University 1st semester Lecture Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/omhkzf69t8m8576/Technical_English.pdf'),
(50, 'HS6151 Question bank link 2', 'HS6151 Technical English I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nHS6151 Technical English I Notes\r\nAnna University HS6151 Technical English I 1st semester notes\r\nHS6151 Technical English I Notes\r\nAnna University HS6151 Notes\r\nHS6151 Technical English I Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  HS6151 English 1 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes \r\nAnna University 1st semester Lecture Notes Regulation 2013\r\nAnna University 1st Semester subject notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/74pj57haxbb4usu/HS-6151-Technical-english-I-PART-B-with-hint.pdf'),
(51, 'HS6151 Question bank link 2', 'HS6151 Technical English I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nHS6151 Technical English I Notes\r\nAnna University HS6151 Technical English I 1st semester notes\r\nHS6151 Technical English I Notes\r\nAnna University HS6151 Notes\r\nHS6151 Technical English I Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  HS6151 English 1 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes \r\nAnna University 1st semester Lecture Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/74pj57haxbb4usu/HS-6151-Technical-english-I-PART-B-with-hint.pdf'),
(52, 'HS6151 Part A (2 marks) with answers', 'HS6151 Technical English I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nHS6151 Technical English I Notes\r\nAnna University HS6151 Technical English I 1st semester notes\r\nHS6151 Technical English I Notes\r\nAnna University HS6151 Notes\r\nHS6151 Technical English I Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  HS6151 English 1 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes \r\nAnna University 1st semester Lecture Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/7j2vy994d77v7t9/HS-6151-Technical-English-I-Part-A.pdf'),
(53, 'HS6151 Part B with Hints material', 'HS6151 Technical English I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nHS6151 Technical English I Notes\r\nAnna University HS6151 Technical English I 1st semester notes\r\nHS6151 Technical English I Notes\r\nAnna University HS6151 Notes\r\nHS6151 Technical English I Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  HS6151 English 1 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes \r\nAnna University 1st semester Lecture Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/74pj57haxbb4usu/HS-6151-Technical-english-I-PART-B-with-hint.pdf'),
(54, 'HS6151 Previous year Question pape', 'HS6151 Technical English I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nHS6151 Technical English I Notes\r\nAnna University HS6151 Technical English I 1st semester notes\r\nHS6151 Technical English I Notes\r\nAnna University HS6151 Notes\r\nHS6151 Technical English I Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  HS6151 English 1 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes \r\nAnna University 1st semester Lecture Notes Regulation 2013', NULL, NULL, 'http://www.rejinpaul.com/2014/06/hs6151-technical-english-i-question-papers-regulation-2013-1st-semester.html'),
(55, 'MA6151 Notes 1', 'MA6151 Mathematics I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nMA6151 Mathematics I Notes\r\nAnna University MA6151 Mathematics I 1st semester notes\r\nMA6151 Mathematics I Notes\r\nAnna University MA6151 Notes\r\nMA6151 Mathematics I Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  MA6151 Maths 1 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/sd88ysy05tcibbd/MA6151_-_Mathematics_-_I_notes.pdf'),
(56, 'MA6151 Notes 2', 'MA6151 Mathematics I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nMA6151 Mathematics I Notes\r\nAnna University MA6151 Mathematics I 1st semester notes\r\nMA6151 Mathematics I Notes\r\nAnna University MA6151 Notes\r\nMA6151 Mathematics I Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  MA6151 Maths 1 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/ujlqxdpgdvt55kk/ma6151_formula.pdf'),
(57, 'MA6151 Formula link ', 'MA6151 Mathematics I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nMA6151 Mathematics I Notes\r\nAnna University MA6151 Mathematics I 1st semester notes\r\nMA6151 Mathematics I Notes\r\nAnna University MA6151 Notes\r\nMA6151 Mathematics I Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  MA6151 Maths 1 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/ujlqxdpgdvt55kk/ma6151_formula.pdf'),
(58, 'MA6151 Question bank link 1', 'MA6151 Mathematics I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nMA6151 Mathematics I Notes\r\nAnna University MA6151 Mathematics I 1st semester notes\r\nMA6151 Mathematics I Notes\r\nAnna University MA6151 Notes\r\nMA6151 Mathematics I Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  MA6151 Maths 1 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/zzlka87qpbt8y8b/MA6151-Mathematics-I.pdf'),
(59, 'MA6151 Question bank link 2', 'MA6151 Mathematics I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nMA6151 Mathematics I Notes\r\nAnna University MA6151 Mathematics I 1st semester notes\r\nMA6151 Mathematics I Notes\r\nAnna University MA6151 Notes\r\nMA6151 Mathematics I Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  MA6151 Maths 1 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/191q9luaszduy6z/ma6151_qb.pdf'),
(60, 'MA6151 Question bank link 3 ', 'MA6151 Mathematics I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nMA6151 Mathematics I Notes\r\nAnna University MA6151 Mathematics I 1st semester notes\r\nMA6151 Mathematics I Notes\r\nAnna University MA6151 Notes\r\nMA6151 Mathematics I Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  MA6151 Maths 1 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/f7wb8oy39j4goch/ma6151_qb2.pdf'),
(61, 'MA6151 Part A (2 marks) with answers ', 'MA6151 Mathematics I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nMA6151 Mathematics I Notes\r\nAnna University MA6151 Mathematics I 1st semester notes\r\nMA6151 Mathematics I Notes\r\nAnna University MA6151 Notes\r\nMA6151 Mathematics I Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  MA6151 Maths 1 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/i8t68j1osrkbmib/ma6151_2_marks.pdf'),
(63, 'PH6151 Notes 1', 'PH6151 Engineering Physics I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nPH6151 Engineering Physics I Notes\r\nAnna University PH6151 Engineering Physics 1 1st semester notes\r\nPH6151 Engineering Chemistry Notes\r\nAnna University PH6151 Notes\r\nPH6151 Engineering Physics Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University PH6151 Engineering Physics 1st Semester Notes\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes \r\nAnna University 1st semester Lecture Notes Regulation 2013\r\nAnna University 1st Semester subject notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/682yh1ajf7p1p2e/PH6151-Engineering_Physics_-_I.pdf'),
(64, 'PH6151 Notes 2 ', 'PH6151 Engineering Physics I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nPH6151 Engineering Physics I Notes\r\nAnna University PH6151 Engineering Physics 1 1st semester notes\r\nPH6151 Engineering Chemistry Notes\r\nAnna University PH6151 Notes\r\nPH6151 Engineering Physics Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University PH6151 Engineering Physics 1st Semester Notes\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes \r\nAnna University 1st semester Lecture Notes Regulation 2013\r\nAnna University 1st Semester subject notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/h61c1n8b0oa8g0d/PH6151-Physics-part-B-with-hint.pdf'),
(65, 'PH6151 Part B with Hints material', 'PH6151 Engineering Physics I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nPH6151 Engineering Physics I Notes\r\nAnna University PH6151 Engineering Physics 1 1st semester notes\r\nPH6151 Engineering Chemistry Notes\r\nAnna University PH6151 Notes\r\nPH6151 Engineering Physics Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University PH6151 Engineering Physics 1st Semester Notes\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes \r\nAnna University 1st semester Lecture Notes Regulation 2013\r\nAnna University 1st Semester subject notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/h61c1n8b0oa8g0d/PH6151-Physics-part-B-with-hint.pdf'),
(66, 'PH6151 Previous year Question paper', 'PH6151 Engineering Physics I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nPH6151 Engineering Physics I Notes\r\nAnna University PH6151 Engineering Physics 1 1st semester notes\r\nPH6151 Engineering Chemistry Notes\r\nAnna University PH6151 Notes\r\nPH6151 Engineering Physics Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University PH6151 Engineering Physics 1st Semester Notes\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes \r\nAnna University 1st semester Lecture Notes Regulation 2013\r\nAnna University 1st Semester subject notes Regulation 2013', NULL, NULL, 'http://www.rejinpaul.com/2014/06/ph6151-engineering-physics-i-question-papers-previous-year-regulation-2013-1st-semester.html'),
(67, 'CY6151 Notes', 'CY6151 Engineering Chemistry I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nCY6151 Engineering Chemistry I Notes\r\nAnna University CY6151 Engineering Chemistry 1 1st semester notes\r\nCY6151 Engineering Chemistry Notes\r\nAnna University CY6151 Notes\r\nCY6151 Engineering Chemistry Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University CY6151 Engineering Chemistry 1st Semester Notes\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes \r\nAnna University 1st semester Lecture Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/04y14amahwuncl8/CY6151-CHEM-PART-B-with-hint.pdf'),
(68, 'CY6151 Question bank', 'CY6151 Engineering Chemistry I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nCY6151 Engineering Chemistry I Notes\r\nAnna University CY6151 Engineering Chemistry 1 1st semester notes\r\nCY6151 Engineering Chemistry Notes\r\nAnna University CY6151 Notes\r\nCY6151 Engineering Chemistry Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University CY6151 Engineering Chemistry 1st Semester Notes\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes \r\nAnna University 1st semester Lecture Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/xsn9syns8amtpa8/CY6151-Engineering_Chemistry-I.pdf'),
(69, 'CY6151 PArt A (2 marks) with answers', 'CY6151 Engineering Chemistry I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nCY6151 Engineering Chemistry I Notes\r\nAnna University CY6151 Engineering Chemistry 1 1st semester notes\r\nCY6151 Engineering Chemistry Notes\r\nAnna University CY6151 Notes\r\nCY6151 Engineering Chemistry Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University CY6151 Engineering Chemistry 1st Semester Notes\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes \r\nAnna University 1st semester Lecture Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/0x59sn7t7ckktwt/CY6151-Chemsitry-Part-A.pdf'),
(70, 'CY6151 Part B with Hints material ', 'CY6151 Engineering Chemistry I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nCY6151 Engineering Chemistry I Notes\r\nAnna University CY6151 Engineering Chemistry 1 1st semester notes\r\nCY6151 Engineering Chemistry Notes\r\nAnna University CY6151 Notes\r\nCY6151 Engineering Chemistry Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University CY6151 Engineering Chemistry 1st Semester Notes\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes \r\nAnna University 1st semester Lecture Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/04y14amahwuncl8/CY6151-CHEM-PART-B-with-hint.pdf'),
(71, 'CY6151 Previous year Question paper', 'CY6151 Engineering Chemistry I Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nCY6151 Engineering Chemistry I Notes\r\nAnna University CY6151 Engineering Chemistry 1 1st semester notes\r\nCY6151 Engineering Chemistry Notes\r\nAnna University CY6151 Notes\r\nCY6151 Engineering Chemistry Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University CY6151 Engineering Chemistry 1st Semester Notes\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes \r\nAnna University 1st semester Lecture Notes Regulation 2013', NULL, NULL, 'http://www.rejinpaul.com/2014/06/cy6151-engineering-chemistry-i-question-papers-previous-year-regulation-2013-1st-semester.html'),
(72, 'GE6151 Notes 1', 'GE6151 Computing Programming Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nGE6151 Computing Programming Notes\r\nAnna University GE6151 Computing Programming I 1st semester notes\r\nGE6151 Computing Programming Notes\r\nAnna University GE6151 Notes\r\nGE6151 Computing Programming Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  GE6151 Computing Programming 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/194c1eup2k446tb/cp%20notes.pdf'),
(73, 'GE6151 Notes 2 ', 'GE6151 Computing Programming Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nGE6151 Computing Programming Notes\r\nAnna University GE6151 Computing Programming I 1st semester notes\r\nGE6151 Computing Programming Notes\r\nAnna University GE6151 Notes\r\nGE6151 Computing Programming Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  GE6151 Computing Programming 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/hv73w96d3v6tsb6/GE6151_uw.pdf'),
(74, 'GE6151 Notes 3', 'GE6151 Computing Programming Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nGE6151 Computing Programming Notes\r\nAnna University GE6151 Computing Programming I 1st semester notes\r\nGE6151 Computing Programming Notes\r\nAnna University GE6151 Notes\r\nGE6151 Computing Programming Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  GE6151 Computing Programming 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/377uz4jwxt4qg4l/CP_5_UNITS_NOTES.pdf');
INSERT INTO `book` (`BID`, `BTITLE`, `KEYWORDS`, `AUTHOR`, `CATEGORY`, `FILE`) VALUES
(75, 'GE6151 Question bank link 1', 'GE6151 Computing Programming Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nGE6151 Computing Programming Notes\r\nAnna University GE6151 Computing Programming I 1st semester notes\r\nGE6151 Computing Programming Notes\r\nAnna University GE6151 Notes\r\nGE6151 Computing Programming Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  GE6151 Computing Programming 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/z3i5n5tp6b40n92/GE6151-Computer_Programming.pdf'),
(76, 'GE6151 Question bank link 2', 'GE6151 Computing Programming Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nGE6151 Computing Programming Notes\r\nAnna University GE6151 Computing Programming I 1st semester notes\r\nGE6151 Computing Programming Notes\r\nAnna University GE6151 Notes\r\nGE6151 Computing Programming Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  GE6151 Computing Programming 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/f60kuo6dc5oqavk/GE6151_qb.pdf'),
(77, 'GE6151 Part A (2 marks) with answers', 'GE6151 Computing Programming Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nGE6151 Computing Programming Notes\r\nAnna University GE6151 Computing Programming I 1st semester notes\r\nGE6151 Computing Programming Notes\r\nAnna University GE6151 Notes\r\nGE6151 Computing Programming Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  GE6151 Computing Programming 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013', NULL, NULL, 'http://www.mediafire.com/download/x18efifd1sixlyc/CP_2_MARK_AND_16_MARK.pdf'),
(78, 'GE6152 Notes 1', 'GE6152 Engineering Graphics Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nGE6152 Engineering Graphics Notes\r\nAnna University GE6152 Engineering Graphics I 1st semester notes\r\nGE6152 Engineering Graphics Notes\r\nAnna University GE6152 Notes\r\nGE6152 Engineering Graphics Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  GE6152 Engineering Graphics 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes ', NULL, NULL, 'http://www.mediafire.com/download/pc9cob2a28l9l8o/eg_notes.pdf'),
(79, 'GE6152 Notes 2', 'GE6152 Engineering Graphics Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nGE6152 Engineering Graphics Notes\r\nAnna University GE6152 Engineering Graphics I 1st semester notes\r\nGE6152 Engineering Graphics Notes\r\nAnna University GE6152 Notes\r\nGE6152 Engineering Graphics Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  GE6152 Engineering Graphics 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes ', NULL, NULL, 'http://www.mediafire.com/download/pc9cob2a28l9l8o/eg_notes.pdf'),
(80, 'GE6152 Question bank ', 'GE6152 Engineering Graphics Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nGE6152 Engineering Graphics Notes\r\nAnna University GE6152 Engineering Graphics I 1st semester notes\r\nGE6152 Engineering Graphics Notes\r\nAnna University GE6152 Notes\r\nGE6152 Engineering Graphics Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  GE6152 Engineering Graphics 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes ', NULL, NULL, 'http://www.mediafire.com/download/efiwxcfx3tzjzhb/GE6152_qb.pdf'),
(81, 'GE6152 Part B with Hints material', 'GE6152 Engineering Graphics Anna university 1st semester Regulation 2013 notes\r\nAnna university UG Notes\r\nAnna University 1st Semester Notes\r\nRegulation 2013 1st Semester Notes Anna University\r\nGE6152 Engineering Graphics Notes\r\nAnna University GE6152 Engineering Graphics I 1st semester notes\r\nGE6152 Engineering Graphics Notes\r\nAnna University GE6152 Notes\r\nGE6152 Engineering Graphics Lecture Notes Subject Notes Anna University\r\nAnna University 1st Semester notes\r\nAnna University chennai madurai,coimbatore 1st semester notes\r\nAnna University First Semester notes \r\nAnna University  GE6152 Engineering Graphics 1st Semester Notes common for all departments\r\nAnna University 1st Semester Notes Regulation 2013\r\nAnna University 1st Semester Notes ', NULL, NULL, 'http://www.mediafire.com/download/sb9aps3eikvdomy/GE-6152-Engineering-Graphics-Part-B.pdf'),
(82, 'MA2262 Probability and Queueing Theory Notes PQT Notes anna University', 'MA2262 Probability and Queueing Theory Notes PQT Notes anna University', NULL, NULL, 'http://www.mediafire.com/download/zz7u6xa0d3h3mgg/pqt-anna-university-notes-rejinpaul.pdf'),
(85, 'UNIT I  LECTURER NOTES', '\r\nCS2251 Design and Analysis of Algorithms Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5', NULL, NULL, 'http://www.mediafire.com/?5y58g1rch5lj3bb'),
(86, 'UNIT II  LECTURER NOTES    ', '\r\nCS2251 Design and Analysis of Algorithms Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5', NULL, NULL, 'http://www.mediafire.com/?gdv5m8ijzdeod8u'),
(87, 'UNIT 3 LECTURER NOTES  ', '\r\nCS2251 Design and Analysis of Algorithms Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5', NULL, NULL, 'http://www.mediafire.com/?tw00szzt3087j7m'),
(88, 'UNIT IV LECTURER NOTES   ', '\r\nCS2251 Design and Analysis of Algorithms Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5', NULL, NULL, 'http://www.mediafire.com/?9irporxtxl52j2p'),
(89, 'UNIT V LECTURER NOTES        ', '\r\nCS2251 Design and Analysis of Algorithms Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5', NULL, NULL, 'http://www.mediafire.com/?xy2198jjdrqslms'),
(90, 'UNIT I  LECTURER NOTES   ', '\r\nCS 2252 Microprocessors and Microcontrollers Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5', NULL, NULL, 'http://www.mediafire.com/?osqwobkpfy2w3qk'),
(91, 'UNIT II  LECTURER NOTES ', '\r\nCS 2252 Microprocessors and Microcontrollers Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5', NULL, NULL, 'http://www.mediafire.com/?osqwobkpfy2w3qk'),
(92, 'UNIT III LECTURER NOTES', '\r\nCS 2252 Microprocessors and Microcontrollers Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5', NULL, NULL, 'http://www.mediafire.com/?osqwobkpfy2w3qk'),
(93, 'UNIT IV LECTURER NOTES ', '\r\nCS 2252 Microprocessors and Microcontrollers Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5', NULL, NULL, 'http://www.mediafire.com/?osqwobkpfy2w3qk'),
(94, 'UNIT V LECTURER NOTES     ', '\r\nCS 2252 Microprocessors and Microcontrollers Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5', NULL, NULL, 'http://www.blogger.com/goog_764018636'),
(95, 'CS2253 Computer Organization and Architecture Notes (COA Notes) anna University', 'CS2253 Computer Organization and Architecture Notes (COA Notes) anna University', NULL, NULL, 'http://www.mediafire.com/download/9npj9x5j7oxods7/CS2253-Notes%20-%20Rejinpaul(2).pdf'),
(96, 'CS2254 Operating Systems Notes (OS Notes)anna University', 'CS2254 Operating Systems Notes (OS Notes)anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/wkkaz2dffibjb0g/OS%20notes%20rejinpaul.zip'),
(97, 'CS2255 Database Management Systems Notes (DBMS Notes) anna University', 'CS2255 Database Management Systems Notes (DBMS Notes) anna University', NULL, NULL, 'http://www.mediafire.com/download/3w0a0e3q4nncjwb/DBMS%20Notes%20rejinpaul.pdf'),
(98, 'UNIT I  LECTURER NOTES ', 'CS 2351 Artificial Intelligence Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5 .\r\nM', NULL, NULL, 'http://www.mediafire.com/?pe5a6izp15ha3mv'),
(99, 'UNIT I PART II NOTES', 'CS 2351 Artificial Intelligence Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5 ', NULL, NULL, 'http://www.mediafire.com/?t7lhfiph3pmi717'),
(100, 'UNIT II LECTURER NOTES ', 'CS 2351 Artificial Intelligence Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5 ', NULL, NULL, 'http://www.mediafire.com/?a1pgczdm09gc6n2'),
(101, 'UNIT 3 LECTURER NOTES     ', 'CS 2351 Artificial Intelligence Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5 ', NULL, NULL, 'http://www.mediafire.com/?ab05q5rz06eif1c'),
(102, 'UNIT IV LECTURER NOTES     ', 'CS 2351 Artificial Intelligence Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5 ', NULL, NULL, 'http://www.blogger.com/goog_119539617'),
(103, 'QUESTION BANK ALL UNITS', 'CS 2351 Artificial Intelligence Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5  qb', NULL, NULL, 'http://www.mediafire.com/?6i31d0xr8xd61r8'),
(104, ' TWO MARKS QUESTIONS', 'CS 2351 Artificial Intelligence Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5 ', NULL, NULL, 'http://www.mediafire.com/?mk3bp0fakza4adm'),
(105, 'UNIT I  LECTURER NOTES', 'CS2352 Principles of Compiler Design Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5\r\n', NULL, NULL, 'http://www.mediafire.com/?xhi9yid8w2entet'),
(106, 'UNIT II  LECTURER NOTES  ', 'CS2352 Principles of Compiler Design Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5\r\n', NULL, NULL, 'http://www.mediafire.com/?crvd328j5ilffaf'),
(107, 'UNIT 3 LECTURER NOTES   ', 'CS2352 Principles of Compiler Design Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5\r\n', NULL, NULL, 'http://www.mediafire.com/?yxmmq28ub5kgo36'),
(108, 'UNIT IV LECTURER NOTES', 'CS2352 Principles of Compiler Design Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5\r\n', NULL, NULL, 'http://www.mediafire.com/?a1mjcpbzluwxcss'),
(109, 'ASSIGNMENT WITH SOLUTION ', 'CS2352 Principles of Compiler Design Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5\r\n', NULL, NULL, 'http://www.mediafire.com/?wtuxfj0l5xd2c55'),
(110, 'EXAMPLES WITH SOLUTION', 'CS2352 Principles of Compiler Design Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5\r\n', NULL, NULL, 'http://www.mediafire.com/?707gi18gfds2293'),
(111, ' CS2353 Object Oriented Analysis and Design Notes (OOAD Notes) Anna University', '\r\nCS2353 Object Oriented Analysis and Design Notes (OOAD Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/z9cjd84ag9z7qvc/OOAD%20Notes%20Rejinpaul.rar'),
(112, 'Chapter 1', 'CS2354 Advanced Computer Architecture Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5\r\n', NULL, NULL, 'http://www.mediafire.com/?ds2jvi7d518h62y'),
(113, 'Chapter 2', 'CS2354 Advanced Computer Architecture Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5\r\n', NULL, NULL, 'http://www.mediafire.com/?rd8y4ewzyg0yvll'),
(114, 'Chapter 3', 'CS2354 Advanced Computer Architecture Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5\r\n', NULL, NULL, 'http://www.mediafire.com/?i5knonou1g31tit'),
(115, 'Chapter 4', 'CS2354 Advanced Computer Architecture Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5\r\n', NULL, NULL, 'http://www.mediafire.com/?cqtm0t6qmtzuj6b'),
(116, 'Chapter 5', 'CS2354 Advanced Computer Architecture Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5\r\n', NULL, NULL, 'http://www.mediafire.com/?qfb257jl4pijusz'),
(117, 'Chapter 6', 'CS2354 Advanced Computer Architecture Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5\r\n', NULL, NULL, 'http://www.mediafire.com/?f6cl46cb3uddknj'),
(118, 'Chapter 7', 'CS2354 Advanced Computer Architecture Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5\r\n', NULL, NULL, 'http://www.mediafire.com/?s7a6v2zy3cpxak0'),
(119, 'Chapter 8', 'CS2354 Advanced Computer Architecture Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5\r\n', NULL, NULL, 'http://www.mediafire.com/?qiy30v6rxdxlqmt'),
(120, 'CS2021 Multicore Programming Notes (MP Notes) Anna University unit 1', 'CS2021 Multicore Programming Notes (MP Notes) Anna University ', NULL, NULL, 'https://docs.google.com/viewer?a=v&pid=sites&srcid=ZWR1bm90ZXMuaW58Y29tcHV0ZXItc2NpZW5jZS1sZWN0dXJlLW5vdGVzfGd4OjcxNjMzMTY1MzRjYzUzNmY'),
(121, 'CS2021 Multicore Programming Notes (MP Notes) Anna University  unit 2', 'CS2021 Multicore Programming Notes (MP Notes) Anna University ', NULL, NULL, 'https://docs.google.com/viewer?a=v&pid=sites&srcid=ZWR1bm90ZXMuaW58Y29tcHV0ZXItc2NpZW5jZS1sZWN0dXJlLW5vdGVzfGd4OjI0NDUyMGE1ZDBmMmRhODY'),
(122, 'CS2021 Multicore Programming Notes (MP Notes) Anna University  Unit 3', 'CS2021 Multicore Programming Notes (MP Notes) Anna University ', NULL, NULL, 'https://docs.google.com/viewer?a=v&pid=sites&srcid=ZWR1bm90ZXMuaW58Y29tcHV0ZXItc2NpZW5jZS1sZWN0dXJlLW5vdGVzfGd4OjZkMzRlZWJjNDQzZTNmMDU'),
(123, 'CS2021 Multicore Programming Notes (MP Notes) Anna University  Unit 4', 'CS2021 Multicore Programming Notes (MP Notes) Anna University ', NULL, NULL, 'https://docs.google.com/viewer?a=v&pid=sites&srcid=ZWR1bm90ZXMuaW58Y29tcHV0ZXItc2NpZW5jZS1sZWN0dXJlLW5vdGVzfGd4OjczZTc4ZjRhMDRmOWRk'),
(124, 'CS2021 Multicore Programming Notes (MP Notes) Anna University  Unit 5', 'CS2021 Multicore Programming Notes (MP Notes) Anna University ', NULL, NULL, 'https://docs.google.com/viewer?a=v&pid=sites&srcid=ZWR1bm90ZXMuaW58Y29tcHV0ZXItc2NpZW5jZS1sZWN0dXJlLW5vdGVzfGd4OjEwMGUwN2QyZTdiNmI5ZTE'),
(125, 'CS2022 Visual Programming Notes (VP Notes) Anna University Unit !', 'CS2022 Visual Programming Notes (VP Notes) Anna University', NULL, NULL, 'https://docs.google.com/viewer?a=v&pid=sites&srcid=ZWR1bm90ZXMuaW58Y29tcHV0ZXItc2NpZW5jZS1sZWN0dXJlLW5vdGVzfGd4OjcxNjMzMTY1MzRjYzUzNmY'),
(126, 'CS2022 Visual Programming Notes (VP Notes) Anna University Unit 2', 'CS2022 Visual Programming Notes (VP Notes) Anna University', NULL, NULL, 'https://docs.google.com/viewer?a=v&pid=sites&srcid=ZWR1bm90ZXMuaW58Y29tcHV0ZXItc2NpZW5jZS1sZWN0dXJlLW5vdGVzfGd4OjI0NDUyMGE1ZDBmMmRhODY'),
(127, 'CS2022 Visual Programming Notes (VP Notes) Anna University Unit 3', 'CS2022 Visual Programming Notes (VP Notes) Anna University ', NULL, NULL, 'https://docs.google.com/viewer?a=v&pid=sites&srcid=ZWR1bm90ZXMuaW58Y29tcHV0ZXItc2NpZW5jZS1sZWN0dXJlLW5vdGVzfGd4OjZkMzRlZWJjNDQzZTNmMDU'),
(128, 'CS2022 Visual Programming Notes (VP Notes) Anna University Unit 4', 'CS2022 Visual Programming Notes (VP Notes) Anna University', NULL, NULL, 'https://docs.google.com/viewer?a=v&pid=sites&srcid=ZWR1bm90ZXMuaW58Y29tcHV0ZXItc2NpZW5jZS1sZWN0dXJlLW5vdGVzfGd4OjczZTc4ZjRhMDRmOWRk'),
(129, 'CS2022 Visual Programming Notes (VP Notes) Anna University Unit 5', 'CS2022 Visual Programming Notes (VP Notes) Anna University', NULL, NULL, 'https://docs.google.com/viewer?a=v&pid=sites&srcid=ZWR1bm90ZXMuaW58Y29tcHV0ZXItc2NpZW5jZS1sZWN0dXJlLW5vdGVzfGd4OjEwMGUwN2QyZTdiNmI5ZTE'),
(130, 'CS2023 Advanced JAVA Programming Notes (AJP Notes) Anna University', 'CS2023 Advanced JAVA Programming Notes (AJP Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/kfn6t8899us8azq/AJP%20Notes%20rejinpaul.pdf'),
(131, 'IT2353 Web Technology Notes (WT Notes) Anna University', 'IT2353 Web Technology Notes (WT Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/?481wi25sw3616i2'),
(132, 'CS2028 UNIX Internals Notes (UI Notes) Anna University', 'CS2028 UNIX Internals Notes (UI Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/1dhixjt9kgp3671/cs2029_notes_-_rejinpaul.pdf'),
(133, 'IT2354 Embedded Systems Notes (ES Notes) Anna University', 'IT2354 Embedded Systems Notes (ES Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/?4dkvae2utatapmy'),
(134, 'CS2029 Advanced Database Technology Notes (ADT Notes) Anna University', 'CS2029 Advanced Database Technology Notes (ADT Notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/1dhixjt9kgp3671/cs2029_notes_-_rejinpaul.pdf'),
(135, ' IT2023 Digital Image Processing Notes (DIP Notes) Anna University', '\r\nIT2023 Digital Image Processing Notes (DIP Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/ua2g4wg1x4uub81/DIP_Notes_2.pdf'),
(136, 'CS2055 Software Quality Assurance Notes (SQA Notes) Anna University', 'CS2055 Software Quality Assurance Notes (SQA Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/?qzyudykymaoq767uxuurrwbcyyazzfz'),
(137, 'CS2055 SQA Notes 2', 'CS2055 SQA Notes 2', NULL, NULL, 'http://www.mediafire.com/download/vh6r2rxtab9tbol/cs2055_sqa_notes_rejinpaul.pdf'),
(138, 'CS2056 Distributed Systems Notes (DS Notes) Anna University', 'CS2056 Distributed Systems Notes (DS Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/7l85t9f80c77z80/cs2056_ds_notes_rejinpaul.pdf'),
(139, 'GE2025 Professional Ethics in Engineering Notes (PEE Notes) Anna University', 'GE2025 Professional Ethics in Engineering Notes (PEE Notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/3g1g1zjgvoqfay9/GE2021_GE2025_Notes.doc'),
(140, 'CS2401 Computer Graphics Notes 2', 'CS2401 Computer Graphics Notes 2', NULL, NULL, 'http://www.mediafire.com/download/e7r3a5zjhafp1z5/cs2401_cg_notes_rejinpaul.pdf'),
(141, 'download CS2402 Notes MPC Notes ', 'download CS2402 Notes MPC Notes ', NULL, NULL, 'http://www.mediafire.com/download/dcx83apxpignxgj/cs2402_mpc_notes_rejinpaul.pdf'),
(143, 'CS2403 Digital Signal Processing', 'download CS2403 Notes DSP NotesCS2403 Digital Signal Processing', NULL, NULL, 'http://www.mediafire.com/download/5rnuwcdps5v16kl/cs2403_dsp_notes_rejinpaul.pdf'),
(144, 'CS2032 Data Warehousing and Data Mining', 'download CS2032 Notes DWDM Notes - Click here\r\nCS2032 Notes 2 Click here (NEW !!)\r\nCS2032 Data Warehousing and Data Mining', NULL, NULL, 'http://www.mediafire.com/download/j1j5blrh8scjguw/cs2032_dwdm_notes_rejinpaul.pdf'),
(145, 'IT2352 Cryptography and Network Security Anna university ', 'IT2352 Cryptography and Network Security Anna university Question bank question paper previous year question paper unit wise unit 1,unit 2 unit 3 unit 4 unit 5\r\n', NULL, NULL, 'http://www.mediafire.com/?3rynhhw5wnn0uhh'),
(146, 'GE2022 Total Quality Management Notes GE2022 TQM Notes', 'GE2022 Total Quality Management Notes GE2022 TQM Notes', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/GE2022%20TQM-Notes.pdf?attredirects=0&d=1'),
(147, 'IT2351 Network Programming and Management', 'IT2351 Network Programming and Management', NULL, NULL, 'http://www.mediafire.com/?e24v928m5s7t4w1'),
(148, 'IT2032 Software Testing', 'IT2032 Software Testing st', NULL, NULL, 'http://www.mediafire.com/download/zdtjy3ts0xafk38/Software_Testing_notes.pdf'),
(149, 'IT2032 Software Testing 2', 'IT2032 Software Testing', NULL, NULL, 'http://www.mediafire.com/download/e8c7h8somhhqy9n/IT2032_ST_notes_rejinpaul.pdf'),
(150, 'CS2041 C# and .NET Framework Notes', 'CS2041 C# and .NET Framework Notes', NULL, NULL, 'http://www.mediafire.com/download/1fgi7gyza161y2m/CS2041_Notes.rar'),
(151, 'CS2041 C# and .NET Framework Notes', 'CS2041 C# and .NET Framework Notes', NULL, NULL, 'http://www.mediafire.com/download/0qftl1c9l5tckb2/cs2041_c#_notes_rejinpaul.pdf'),
(152, 'CS2055 Software Quality Assurance Notes', 'CS2055 Software Quality Assurance Notes', NULL, NULL, 'http://www.mediafire.com/download/vh6r2rxtab9tbol/cs2055_sqa_notes_rejinpaul.pdf'),
(154, 'MA2261 Probability and Random Processes Notes (M4 notes – PRP) Anna University', 'MA2261 Probability and Random Processes Notes (M4 notes – PRP) Anna University', NULL, NULL, 'http://www.mediafire.com/download/u7smddeirvytxnb/prp_notes_rejinpaul.pdf'),
(155, 'EC2251 Electronic Circuits 2 Notes (EC 2 notes) Anna University', 'EC2251 Electronic Circuits 2 Notes (EC 2 notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/jli7ct4ij2g0wvp/Electronic_Circuits_II.pdf'),
(156, 'EC2252 Communication Theory Notes (CT) Anna University', 'EC2252 Communication Theory Notes (CT) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/2sdsh3nqel1ke4n/EC2252-NOL.ppt'),
(157, 'EC2253 Electromagnetic Fields EMF notes Anna University', 'EC2253 Electromagnetic Fields EMF notes Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/fbceu58l4htvacu/Electro_Magetic_Fields_(EMF)NOTES.pdf'),
(158, 'EC2254 Linear Integrated Circuits ( LIC notes) Anna University', 'EC2254 Linear Integrated Circuits ( LIC notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/yz0acb2xfyug4ml/EC_2254_LINEAR_INTEGRATED_CIRCUITS_Lecture_Notes.pdf'),
(159, 'EC2255 Control Systems Notes ( CS notes) Anna University', 'EC2255 Control Systems Notes ( CS notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/8ed2a00cq362d8q/EC2255-notes.pdf'),
(160, ' MG2351 Principles of Management Notes MG2351 POM Notes', ' MG2351 Principles of Management Notes MG2351 POM Notes', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/Principles-of-Mangement-MG2351%20notes.pdf?attredirects=0&d=1'),
(161, 'EC2351 Measurements and Instrumentation MI notes Anna University', 'EC2351 Measurements and Instrumentation MI notes Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/ujspej12yh17v80/EC2351_notes.pdf'),
(162, 'EC2352 Computer Networks notes (CN notes) Anna University', 'EC2352 Computer Networks notes (CN notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/vqgwxdw6tw7krwd/EC2352-NOL.rar'),
(163, 'EC2353 Antenna and Wave Propagation notes (AWP notes) Anna University', 'EC2353 Antenna and Wave Propagation notes (AWP notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/1u28d64almpq99j/ec23532_AWP_notes.doc'),
(164, 'EC2354 VLSI Design notes (VLSI notes) Anna University', 'EC2354 VLSI Design notes (VLSI notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/1xge0fc924mgsn4/VLSI-Design.pdf'),
(165, 'EC2022 Operating Systems OS notes Anna University', 'EC2022 Operating Systems OS notes Anna University', NULL, NULL, 'http://www.mediafire.com/download/m62jxawc905ubdb/Operating_systems_notes.pdf'),
(166, 'EC2021 Medical Electronics notes Anna University ', 'EC2021 Medical Electronics notes Anna University ', NULL, NULL, 'http://www.mediafire.com/download/0p1kqg9yr95hhhs/EC2021-Notes.doc'),
(167, 'EC2043 Wireless networks Notes (WN Notes) Anna University', 'EC2043 Wireless networks Notes (WN Notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/8395xagaahfvllg/ec2043-notes.pdf'),
(168, 'EC2045 Satellite Communication Notes (SC Notes) Anna University', 'EC2045 Satellite Communication Notes (SC Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/xv86xa4656svzjf/statellite_communication-Notes.doc'),
(169, 'ELECTRICAL ENGINEERING EC2201 ANNA UNIVERSITY SUBJECT NOTES,QUESTION BANK,QUESTION PAPER, 2 MARKS AND 16 MARKS', 'ELECTRICAL ENGINEERING EC2201 ANNA UNIVERSITY SUBJECT NOTES,QUESTION BANK,QUESTION PAPER, 2 MARKS AND 16 MARKS\r\n', NULL, NULL, 'http://www.mediafire.com/?awoton06ylv4tcv'),
(170, ' EC 2202 Data Structures and Object Oriented Programming in C++ notes - EC2202 Notes - DOWNLOAD', ' EC 2202 Data Structures and Object Oriented Programming in C++ notes - EC2202 Notes - DOWNLOAD', NULL, NULL, 'http://www.mediafire.com/?w5nxrzblnb0r0gc'),
(171, 'EC 2203 Digital Electronics Notes - EC2203 DE Notes ', 'EC 2203 Digital Electronics Notes - EC2203 DE Notes ', NULL, NULL, 'https://docs.google.com/uc?export=download&id=0BzEWOcXjBCbISzlYRHIyYlN6SkU'),
(172, 'EC 2204 Signals and systems Notes (EC2204 SS Notes)', 'EC 2204 Signals and systems Notes (EC2204 SS Notes)', NULL, NULL, 'https://docs.google.com/uc?export=download&id=0BzEWOcXjBCbIVy02bm9FX1d6b0k'),
(173, 'EC 2204 Signals and systems Notes (EC2204 SS Notes)', 'EC 2204 Signals and systems Notes (EC2204 SS Notes)', NULL, NULL, 'https://docs.google.com/uc?export=download&id=0BzEWOcXjBCbIUGNSVkR6alMxSUE'),
(174, 'EC 2205 Electronic Circuits- I Notes (EC2205 EC 1 Notes) -', 'EC 2205 Electronic Circuits- I Notes (EC2205 EC 1 Notes) -', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/EC2205%20notes.pdf?attredirects=0&d=1'),
(175, ' EC2301 Digital Communication Notes DC  Notes', ' EC2301 Digital Communication Notes DC  Notes', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/Digital%20Communication%20notes.pdf?attredirects=0&d=1'),
(176, ' EC2302 Digital Signal Processing Notes DSP for ECE Notes', ' EC2302 Digital Signal Processing Notes DSP for ECE Notes', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/Digital%20signal%20processing.pdf?attredirects=0&d=1'),
(177, 'EC2303 Computer Architecture and Organization Notes CAO Notes', 'EC2303 Computer Architecture and Organization Notes CAO Notes', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/Computer%20Architecture%20and%20Organization%20notes.pdf?attredirects=0&d=1'),
(178, ' EC2305 Transmission Lines and Waveguides Notes TLW Notes ', ' EC2305 Transmission Lines and Waveguides Notes TLW Notes ', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/Transmission%20lines%20and%20Waveguides%20notes.pdf?attredirects=0&d=1'),
(179, ' EC2304 Microprocessor and Microprocessor Notes MPMC Notes', ' EC2304 Microprocessor and Microprocessor Notes MPMC Notes', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/Microprocessor%20%26%20Microcontroller%20Notes.pdf?attredirects=0&d=1'),
(180, 'ME2151 Engineering Mechanics notes (EM notes - ME2151 Notes )', 'ME2151 Engineering Mechanics notes (EM notes - ME2151 Notes )', NULL, NULL, 'http://www.mediafire.com/?cgg01nia585x4zr'),
(181, 'EE2251 Electrical Machines – I Notes (EM I EM 1 Notes) Anna University', 'EE2251 Electrical Machines – I Notes (EM I EM 1 Notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/o89mtsjahjyfmjk/EE2251_notes.rar'),
(182, 'EE2252 Power Plant Engineering Notes (PPE Notes) Anna University', 'EE2252 Power Plant Engineering Notes (PPE Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/2vdl3zx3rdl2al2/EE2252_notes.pdf'),
(183, 'EE2253 Control Systems Notes (CS Notes) Anna University', 'EE2253 Control Systems Notes (CS Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/57y5g7n8zgnkky8/EE2253_notes.pdf'),
(184, 'EE2254 Linear Integrated Circuits and Applications Notes (LICA Notes) Anna University', 'EE2254 Linear Integrated Circuits and Applications Notes (LICA Notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/md2f229bck7fsif/EE2254_Notes.pdf'),
(185, 'EE2255 Digital Logic Circuits Notes (DLC Notes) Anna University', 'EE2255 Digital Logic Circuits Notes (DLC Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/h4gktt0a6mrvdy8/EE2255_Notes.pdf'),
(186, 'EE2351 Power System Analysis Notes (PSA Notes) Anna University', 'EE2351 Power System Analysis Notes (PSA Notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/4koikvh63bxd8a6/ee2351_notes(2).pdf'),
(187, 'EE2352 Solid State Drives Notes (SSD Notes) Anna University', 'EE2352 Solid State Drives Notes (SSD Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/4koikvh63bxd8a6/ee2351_notes(2).pdf'),
(188, 'EE2353 High Voltage Engineering Notes (HVE Notes) Anna University', 'EE2353 High Voltage Engineering Notes (HVE Notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/jon53ecrppjo74o/hve_notes_eee.pdf'),
(189, 'EE2354 Microprocessors and Micro controller Notes (MM Notes) Anna University', 'EE2354 Microprocessors and Micro controller Notes (MM Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/dj903k8xdv3ulgo/EE2354-NOL.rar'),
(190, 'EE2355 Design of Electrical Machines Notes (DEM Notes) Anna University', 'EE2355 Design of Electrical Machines Notes (DEM Notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/gbohz63gc2qyk6b/design-of-electrical-machines-notes.pdf'),
(191, 'CS2363 Computer Networks Notes (CN Notes) Anna University', 'CS2363 Computer Networks Notes (CN Notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/yclioy0k48gto0v/CS2363_notes.rar'),
(192, 'EI2404 Fibre Optics and Laser Instruments Notes (FOLI Notes) Anna University', 'EI2404 Fibre Optics and Laser Instruments Notes (FOLI Notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/yq95fivkfpa49iv/EE2021-NOL.rar'),
(193, 'GE2025 Professional Ethics in Engineering Notes (PEE Notes) Anna University', 'GE2025 Professional Ethics in Engineering Notes (PEE Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/3g1g1zjgvoqfay9/GE2021_GE2025_Notes.doc'),
(194, 'EE2027 Power System Transients Notes (PST Notes) Anna University', 'EE2027 Power System Transients Notes (PST Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/9nypvzy38pkxdmx/power-system-transients-anna-university-notes.pptx'),
(195, 'EE2028 Power Quality Notes (PQ Notes) Anna University', 'EE2028 Power Quality Notes (PQ Notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/hldbikkrjhlng38/Power_Quality-notes.pdf'),
(196, ' EI2403 VLSI Design Notes Anna University', '\r\nEI2403 VLSI Design Notes Anna University', NULL, NULL, 'http://www.mediafire.com/download/5b5wu3qb7cph2fw/vlsi_design-notes.pdf'),
(197, 'EE2032 High Voltage Direct Current Transmission Notes (HVDC Notes) Anna University', 'EE2032 High Voltage Direct Current Transmission Notes (HVDC Notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/680dcyh19bk426r/HVDC_notes.pdf'),
(198, '  EC2311 Commnication Engineering Notes CE Notes', '\r\n EC2311 Commnication Engineering Notes CE Notes', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/EC2311CommunicationEngineeringnotes.doc?attredirects=0&d=1'),
(199, 'EC2314 Digital Signal Processing Notes DSP Notes ', 'EC2314 Digital Signal Processing Notes DSP Notes ', NULL, NULL, 'http://205.196.122.233/r5cmsmpmpiyg/k6k2vmf8370gyiv/DSP+-+2012+Revision+Notes.docx'),
(200, ' CS2311 Object Oriented Programming notes - CS2311 OOP Notes ', ' CS2311 Object Oriented Programming notes - CS2311 OOP Notes ', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/OOP%20notes%20CS2311%20Notes.pdf?attredirects=0&d=1'),
(201, ' EE2301 Power Electronics Notes PE Notes', ' EE2301 Power Electronics Notes PE Notes', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/POWER-ELECTRONICS-NOTES.pdf?attredirects=0&d=1'),
(202, ' EE2301 Power Electronics Notes - EE2301 PE Notes', ' EE2301 Power Electronics Notes - EE2301 PE Notes', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/POWER-ELECTRONICS-NOTES.pdf?attredirects=0&d=1'),
(203, 'EE2302 Electrical Machines II Notes (EE2202 EM 2 Notes) ', 'EE2302 Electrical Machines II Notes (EE2202 EM 2 Notes) ', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/EE2302%20notes.pdf?attredirects=0&d=1'),
(204, 'EE2303 Transmission & Distribution Notes (EE2303 T & D Notes)', 'EE2303 Transmission & Distribution Notes (EE2303 T & D Notes)', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/ee2303%20notes.pdf?attredirects=0&d=1'),
(206, ' CS2203 Object Oriented Programming Notes OOP Notes', ' CS2203 Object Oriented Programming Notes OOP Notes', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/CS2203%20Object%20Oriented%20Programming%20notes.pdf?attredirects=0&d=1'),
(207, 'MA2262 Probability and Queueing Theory Notes (PQT Notes)Anna University', 'MA2262 Probability and Queueing Theory Notes (PQT Notes)Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/zz7u6xa0d3h3mgg/pqt-anna-university-notes-rejinpaul.pdf'),
(208, 'CS2255 Database Management Systems Notes (DBMS Notes) Anna University  ', 'CS2255 Database Management Systems Notes (DBMS Notes) Anna University\r\n ', NULL, NULL, 'http://www.mediafire.com/download/3w0a0e3q4nncjwb/DBMS%20Notes%20rejinpaul.pdf'),
(210, 'UNIT I  LECTURER NOTES ', 'CS 2252 Microprocessors and Microcontrollers Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5\r\n', NULL, NULL, 'http://www.mediafire.com/?osqwobkpfy2w3qk'),
(211, 'UNIT II  LECTURER NOTES ', 'CS 2252 Microprocessors and Microcontrollers Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5\r\n', NULL, NULL, 'http://www.mediafire.com/?osqwobkpfy2w3qk'),
(212, 'UNIT III LECTURER NOTES   ', 'CS 2252 Microprocessors and Microcontrollers Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5\r\n\r\n', NULL, NULL, 'http://www.mediafire.com/?osqwobkpfy2w3qk'),
(213, 'UNIT IV LECTURER NOTES', 'CS 2252 Microprocessors and Microcontrollers Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5\r\n', NULL, NULL, 'http://www.mediafire.com/?osqwobkpfy2w3qk'),
(214, 'UNIT V LECTURER NOTES              ', 'CS 2252 Microprocessors and Microcontrollers Anna university subject notes lecturer notes unit wise unit 1 , unit 2 unit 3 unit 4 unit 5\r\n', NULL, NULL, 'http://www.blogger.com/goog_764018636'),
(215, 'CS2253 Computer Organization and Architecture Notes (COA Notes) Anna University', 'CS2253 Computer Organization and Architecture Notes (COA Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/9npj9x5j7oxods7/CS2253-Notes%20-%20Rejinpaul(2).pdf'),
(216, 'CS2254 Operating Systems Notes (OS Notes) Anna University', 'CS2254 Operating Systems Notes (OS Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/wkkaz2dffibjb0g/OS%20notes%20rejinpaul.zip'),
(217, 'IT2251 Software Engineering and Quality Assurance Notes (SEQA Notes) Anna University', 'IT2251 Software Engineering and Quality Assurance Notes (SEQA Notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/c96537upi0knv4k/IT2251_Notes.rar'),
(218, 'IT2351 Network Programming and Management Notes (NPM Notes)Anna University', 'IT2351 Network Programming and Management Notes (NPM Notes)Anna University', NULL, NULL, 'http://www.mediafire.com/download/ure3ci8oq2e1cg2/IT2351_notes_1.rar'),
(219, 'CS2353 Object Oriented Analysis and Design Notes (OOAD Notes) Anna University', 'CS2353 Object Oriented Analysis and Design Notes (OOAD Notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/z9cjd84ag9z7qvc/OOAD%20Notes%20Rejinpaul.rar'),
(220, 'IT2352 Cryptography and Network Security Notes (CNS Notes) Anna University', 'IT2352 Cryptography and Network Security Notes (CNS Notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/pvr4cdw3rm5jt12/IT2352_notes.rar'),
(222, 'IT2353 Web Technology Notes (WT Notes) Anna University', 'IT2353 Web Technology Notes (WT Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/?481wi25sw3616i2'),
(223, 'IT2354 Embedded Systems Notes (ES Notes) Anna University', 'IT2354 Embedded Systems Notes (ES Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/?4dkvae2utatapmy'),
(224, 'CS2032 Data Warehousing and Data Mining Notes (DWDM Notes) Anna University | CS2032 Notes 2 Click here', 'CS2032 Data Warehousing and Data Mining Notes (DWDM Notes) Anna University | CS2032 Notes 2 Click here', NULL, NULL, 'http://www.mediafire.com/download/n7w62nnr07t1pni/CS2032_notes.rar'),
(225, 'IT2024 User Interface Design Notes (UID Notes) Anna University', 'IT2024 User Interface Design Notes (UID Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/qfbjki8q3u2qrrw/UID.zip'),
(226, 'IT2023 Digital Image Processing Notes (DIP Notes) Anna University', 'IT2023 Digital Image Processing Notes (DIP Notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/3l7b6yajxo707rk/DIGITAL_IMAGE_PROCESSING_notes.pdf'),
(227, ' IT2301 Java Programming Notes JP Notes', ' IT2301 Java Programming Notes JP Notes', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/IT2301%20notes.rar?attredirects=0&d=1'),
(228, 'MG2452 Engineering Economics and Financial Accounting Notes EEFA Notes', 'MG2452 Engineering Economics and Financial Accounting Notes EEFA Notes', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/MG2452-NOTES.rar?attredirects=0&d=1'),
(229, 'ME2452 EEFA Notes 2', 'MG2452 Engineering Economics and Financial Accounting Notes EEFA Notes', NULL, NULL, 'http://www.mediafire.com/download/p2tsr3ou4434il9/mg2452_eefa_notes_rejinpaul.pdf'),
(230, ' CS2403 Digital Signal Processing Notes DSP Notes for IT', ' CS2403 Digital Signal Processing Notes DSP Notes for IT', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/CS2403-DIGITAL-SIGNAL-PROCESSING%20notes.pdf?attredirects=0&d=1'),
(231, ' IT2302 Information Theory and Coding Notes ITC Notes', ' IT2302 Information Theory and Coding Notes ITC Notes', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/itc-notes.rar?attredirects=0&d=1'),
(232, 'IT2401 Service Oriented Architecture Notes', 'IT2401 Service Oriented Architecture Notes\r\n\r\nDownoad IT2401 Notes SOA Notes', NULL, NULL, 'http://www.mediafire.com/download/6tr5xpxc8aq75rf/IT2401_notes.pdf'),
(233, 'IT2402 Mobile Communication Notes  ', 'IT2402 Mobile Communication Notes  \r\n\r\nDownload IT2402 Notes MC Notes', NULL, NULL, 'http://www.mediafire.com/download/93ty8oxri3rzg56/it2402.pdf'),
(234, 'IT2402 Mobile Communication Notes', 'IT2402 Mobile Communication Notes  \r\n\r\nDownload IT2402 Notes MC Notes ', NULL, NULL, 'http://www.mediafire.com/download/yfz2ix9r5619y2h/IT2402_mc_notes_rejinpaul.pdf'),
(235, 'CS2401 Computer Graphics Notes', 'CS2401 Computer Graphics Notes\r\n\r\n\r\ndownload CS2401 Notes CG Notes', NULL, NULL, 'http://www.mediafire.com/download/e7r3a5zjhafp1z5/cs2401_cg_notes_rejinpaul.pdf'),
(236, 'IT2403 Software Project Management Notes', 'IT2403 Software Project Management Notes\r\n\r\nDownload IT2403 Notes SPM Notes ', NULL, NULL, 'http://www.mediafire.com/download/4yd8i9ooa2oic32/'),
(237, 'CS2029 Advanced Database Technology Notes', 'CS2029 Advanced Database Technology Notes\r\n \r\nDownload CS2029 Notes ADT Note', NULL, NULL, 'http://technicalsymposium.com/IT_SEM7_CS2029NOL.rar'),
(238, 'IT2031 Electronic Commerce Notes', 'IT2031 Electronic Commerce Notes\r\n\r\nDownload IT2031 Notes', NULL, NULL, 'http://www.mediafire.com/download/5pnoro2rexh4bg2/E-commerce.docx'),
(239, 'IT2032 Software Testing Notes', 'IT2032 Software Testing Notes\r\n\r\ndownload IT2032 Notes ST Notes 2 - click here (NEW !!)\r\n', NULL, NULL, 'http://www.mediafire.com/download/e8c7h8somhhqy9n/IT2032_ST_notes_rejinpaul.pdf'),
(240, 'IT2042 Information Security Notes', 'IT2042 Information Security Notes\r\n\r\ndownload IT2042 Notes IS Notes - click here (NEW !!)\r\n', NULL, NULL, 'http://www.mediafire.com/download/9tuas4out6sx2dx/information_security_notes_rejinpaul.pdf'),
(241, 'CS2351 Artificial Intelligence Notes', 'CS2351 Artificial Intelligence Notes\r\n\r\nDownload CS2351 Notes AI Notes', NULL, NULL, 'http://www.mediafire.com/?pe5a6izp15ha3mv'),
(242, 'CS2041 C# and .NET Framework Notes', 'CS2041 C# and .NET Framework Notes\r\n', NULL, NULL, 'http://www.mediafire.com/download/1fgi7gyza161y2m/CS2041_Notes.rar'),
(243, 'CS2041 C# and .NET Framework Notes', 'CS2041 C# and .NET Framework Notes\r\n', NULL, NULL, 'http://www.mediafire.com/download/0qftl1c9l5tckb2/cs2041_c#_notes_rejinpaul.pdf'),
(244, 'CS2055 Software Quality Assurance Notes', 'CS2055 Software Quality Assurance Notes\r\n\r\nCS2055 SQA Notes', NULL, NULL, 'http://www.mediafire.com/download/vh6r2rxtab9tbol/cs2055_sqa_notes_rejinpaul.pdf'),
(245, 'GE2151 Basic Electrical & ElectronicsEngineering notes (BEEE notes - GE2151 Notes) DOWNLOAD', 'GE2151 Basic Electrical & ElectronicsEngineering notes (BEEE notes - GE2151 Notes) DOWNLOAD', NULL, NULL, 'https://sites.google.com/site/anirudhauthor/Lecture%20notes-Basic%20electrical%20and%20electronics%20engineering%20notes.pdf?attredirects=0&d=1'),
(246, 'EC2151 Electric Circuits and Electron Devices notes(EDC/EDEC/ECED notes - EC2151 Notes) - DOWNLOAD ', 'EC2151 Electric Circuits and Electron Devices notes(EDC/EDEC/ECED notes - EC2151 Notes) - DOWNLOAD ', NULL, NULL, 'https://sites.google.com/site/anirudhauthor/EDC%20notes.pdf?attredirects=0&d=1'),
(247, ' GE2152 Basic Civil & Mechanical Engineeringnotes - DOWNLOAD  (BCM notes - GE2152 Notes) ', ' GE2152 Basic Civil & Mechanical Engineeringnotes - DOWNLOAD  (BCM notes - GE2152 Notes) ', NULL, NULL, 'https://sites.google.com/site/anirudhauthor/BCM%20notes.pdf?attredirects=0&d=1'),
(248, 'ME2303 Design of Machine Elements Notes DME Notes', 'ME2303 Design of Machine Elements Notes DME Notes', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/DESIGN%20OF%20MACHINE%20ELEMENTS.pdf?attredirects=0&d=1'),
(249, ' ME2304 Engineering Metrology and Measurements Notes EMM Notes', ' ME2304 Engineering Metrology and Measurements Notes EMM Notes', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/Engineering%20Metrology%20and%20Measurements.pdf?attredirects=0&d=1'),
(250, 'GE2022 Total Quality Management Notes GE2022 TQM Notes ', 'GE2022 Total Quality Management Notes GE2022 TQM Notes ', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/GE2022%20TQM-Notes.pdf?attredirects=0&d=1'),
(251, 'ME 2401 Mechatronics Notes', 'ME 2401 Mechatronics Notes\r\n\r\ndownload ME2401 Notes', NULL, NULL, 'http://www.mediafire.com/download/sveevcj3wf92w70/me2401_notes_rejinpaul.pdf'),
(252, 'ME 2402 Computer Integrated Manufacturing Notes', 'ME 2402 Computer Integrated Manufacturing Notes\r\n\r\ndownload ME2402 Notes CIM Notes', NULL, NULL, 'http://scadec.ac.in/upload/file/CIM%20unitwise%20notes.pdf'),
(253, 'ME 2403 Power Plant Engineering Notes', 'ME 2403 Power Plant Engineering Notes\r\n\r\ndownload ME2403 notes PPE Notes ', NULL, NULL, 'http://www.mediafire.com/download/9kwcptgz9lwco1z/me2403_ppe_notes_rejinpaul.pdf'),
(254, 'ME2027 Process Planning and Cost Estimation Notes', 'ME2027 Process Planning and Cost Estimation Notes\r\n\r\ndownload ME2027 Notes PPCE Notes', NULL, NULL, 'http://www.mediafire.com/download/m9jbqtbjqdmsrf3/me2027_ppce_notes_rejinpaul.pdf'),
(255, 'ME2028 Robotics Notes', 'ME2028 Robotics Notes\r\n\r\ndownload ME2028 Notes ', NULL, NULL, 'http://www.mediafire.com/download/xwqgqnl1yaal7jd/me2028_notes_rejinpaul.pdf'),
(256, 'ME2034 Nuclear Engineering Notes', 'ME2034 Nuclear Engineering Notes\r\n\r\ndownload ME2034 Notes', NULL, NULL, 'http://scadec.ac.in/upload/file/ME%202034%20notes%20nuclear%20engineering.pdf'),
(257, 'MA2266 Statistics and Numerical Methods Notes (NM Notes) Anna University', 'MA2266 Statistics and Numerical Methods Notes (NM Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/595aivwzyzai5jw/ma2266-notes.pdf'),
(258, 'ME2251 Heat and Mass Transfer Notes (HMT Notes) Anna University', 'ME2251 Heat and Mass Transfer Notes (HMT Notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/9un7yeea5egyg8y/HEAT-AND-MASS-TRANSFER-NOTES.pdf'),
(259, 'ME2252 Manufacturing Technology – II Notes (MT 2 Notes) Anna University', 'ME2252 Manufacturing Technology – II Notes (MT 2 Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/gh4ggpbaq0y10xd/ME2252-notes.pdf'),
(260, 'ME2253 Engineering Materials and Metallurgy Notes (EMM Notes) Anna University', 'ME2253 Engineering Materials and Metallurgy Notes (EMM Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/xi5ud99phvib9x5/EMM_Notes.pdf'),
(261, 'ME2254 Strength of Materials Notes (SOM Notes) Anna University', 'ME2254 Strength of Materials Notes (SOM Notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/6pe4aq7ave4iccw/STRENGTH-OF-MATERIALS-notes.pdf'),
(262, 'ME2255 Electronics and Microprocessors Notes (EM Notes) Anna University', 'ME2255 Electronics and Microprocessors Notes (EM Notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/b2zhu767i7ndj8m/electronics-and-micrprocessor-notes.pdf'),
(263, ' MG2351 Principles of Management Notes MG2351 POM Notes', ' MG2351 Principles of Management Notes MG2351 POM Notes', NULL, NULL, 'https://sites.google.com/site/rejinpaul11/Principles-of-Mangement-MG2351%20notes.pdf?attredirects=0&d=1'),
(264, 'ME2351 Gas Dynamics and Jet Propulsion Notes Anna University', 'ME2351 Gas Dynamics and Jet Propulsion Notes Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/pbd3udglbp8eb4h/me2351_notes.pdf'),
(265, 'ME2352 Design of Transmission Systems Notes (DTS Notes) Anna University', 'ME2352 Design of Transmission Systems Notes (DTS Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/i1tqcif7s8l65xj/me2352-notes.pdf'),
(266, 'ME2353 Finite Element Analysis Notes (FEA Notes) Anna University', 'ME2353 Finite Element Analysis Notes (FEA Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/fwv5anqh2599327/ME2353-Finite-Element-Analysis-Lecture-Notes.pdf'),
(267, 'ME2022 Refrigeration & Air conditioning Notes Anna University   ', 'ME2022 Refrigeration & Air conditioning Notes Anna University\r\n  ', NULL, NULL, 'http://www.mediafire.com/download/qarc4b73b3rudd7/refrigiration_and_aircondition_notes.pdf'),
(268, 'MG2451 Engineering Economics and Cost Analysis Notes (EECA Notes) Anna University', 'MG2451 Engineering Economics and Cost Analysis Notes (EECA Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/lqstp8dcd65uzr7/Engineering_Economics_And_Cost_Analysis(3).pdf'),
(269, 'GE2025 Professional Ethics In Engineering Notes (PEE Notes) Anna University', 'GE2025 Professional Ethics In Engineering Notes (PEE Notes) Anna University\r\n', NULL, NULL, 'http://www.mediafire.com/download/3g1g1zjgvoqfay9/GE2021_GE2025_Notes.doc'),
(270, 'ME2034 Nuclear Engineering Notes (NE Notes) Anna University (7th sem)', 'ME2034 Nuclear Engineering Notes (NE Notes) Anna University (7th sem)\r\n', NULL, NULL, 'http://www.mediafire.com/download/37diw65h5dvd6g2/ME2034_notes.doc'),
(271, 'ME2036 Production Planning and Control Notes (PPC Notes) Anna University', 'ME2036 Production Planning and Control Notes (PPC Notes) Anna University', NULL, NULL, 'http://www.mediafire.com/download/sn366zuty48be1u/ME2036-PPC-Lecture-Notes.pdf'),
(272, 'ME2041 Advanced I.C. Engines Notes Anna University', 'ME2041 Advanced I.C. Engines Notes Anna University', NULL, NULL, 'http://www.mediafire.com/download/396pca06600ye9b/advanced-IC-Engine-notes.pdf'),
(274, 'CS6660 Compiler design UNIT 1 UNIT 2', 'Unit-II, unit-2 notes, CS6660 COMPILER DESIGN CD', NULL, NULL, 'https://drive.google.com/open?id=1NwuRlzSGR1IDpw7mCqo6f5fznjUBB2Qv'),
(275, 'CS6660 Compiler design, unit-3 notes', 'Compiler design unit-3 notes CD CS6660 CS 6660', NULL, NULL, 'https://drive.google.com/open?id=1DwoMZfmYnd2IelunphBShUls8qdlhxCC'),
(276, 'Mobile Computing Notes ', 'Mobile Computing Notes  MC unit 1', NULL, NULL, 'https://drive.google.com/open?id=1taI_0IRqMZWK2-c4G9KtVTci_NksD5d5'),
(277, 'CS2254 OS Notes', 'Operating system os CS2254 cs 2254 ', NULL, NULL, 'http://www.mediafire.com/file/wkkaz2dffibjb0g/OS+notes+rejinpaul.zip'),
(280, 'அக்னிச் சிறகுகள் - டாக்டர். அப்துல் கலாம் (Wings of Fire )', 'அக்னிச் சிறகுகள் - டாக்டர். அப்துல் கலாம் (Wings of Fire ) agni siragugal sirakukal siragukal apj abdul kalam bio graphy biography', NULL, NULL, 'https://drive.google.com/open?id=14o0Pw1QPnEDGKB4GW7QxHZOIGq-uBlRx'),
(281, 'Control System  Book - Nagoor Kani (R2013)', 'control system book nagoor kani author systems', NULL, NULL, 'https://drive.google.com/open?id=1woxaGs84Z5HkwnRlQwLr9nxabhsGglIj'),
(287, 'TNPSC-VAO-EXAM-PREVIOUS-YEAR-QUESTION-PAPER-28_02_2016_-GK-', 'TNPSC-VAO-EXAM-PREVIOUS-YEAR-QUESTION-PAPER-28_02_2016_-GK-', NULL, NULL, 'https://drive.google.com/open?id=1SfUxQ8WuVbwb5DmKIJLUAVknxPAwfEvp');
INSERT INTO `book` (`BID`, `BTITLE`, `KEYWORDS`, `AUTHOR`, `CATEGORY`, `FILE`) VALUES
(288, 'TNPSC-VAO-EXAM- QUESTIONS ', 'TNPSC-VAO-EXAM-PREVIOUS-YEAR-QUESTION-PAPER-28_02_2016_-GK- ', NULL, NULL, 'https://drive.google.com/open?id=1qyjF9XRYBaeq-ASn66PXs_FjDw8zhJqR'),
(289, 'Switching Theory And Logic Design ', 'Switching Theory And Logic Design  ', NULL, NULL, 'https://drive.google.com/open?id=1gFwJ104Rfg0jDA_ECxLb4tVuSeH8Gp2Q'),
(290, 'Elements of Computational Fluid Dynamics', 'Elements of Computational Fluid Dynamics', NULL, NULL, 'https://drive.google.com/open?id=1o1dwlGRGTEZDCCTmUhP7A4wQycuELnaC'),
(291, 'EC 6405 Control Systems Lecture Notes', 'EC 6405 Control Systems Lecture Notes ec6405', NULL, NULL, 'https://www.vidyarthiplus.com/vp/attachment.php?aid=23705'),
(292, 'Control Systems Notes Unit 1', 'Control Systems Notes Unit wise cs ic 6501 ', NULL, NULL, 'http://studentsfocus.com/notes/anna_university/EEE/5SEM/IC6501%20-%20CS/notes/105561_2013_regulation.pdf'),
(293, 'Control Systems Notes Unit 2', 'Control Systems Notes Unit  wise CS 6501', NULL, NULL, 'http://studentsfocus.com/notes/anna_university/EEE/5SEM/IC6501%20-%20CS/notes/105562_2013_regulation.pdf'),
(294, 'Control Systems Notes Unit  3', 'Control Systems Notes Unit  wise CS 6501', NULL, NULL, 'http://studentsfocus.com/notes/anna_university/EEE/5SEM/IC6501%20-%20CS/notes/105563_2013_regulation.pdf'),
(295, 'Control Systems Notes Unit  4', 'Control Systems Notes Unit  wise CS 6501', NULL, NULL, 'http://studentsfocus.com/notes/anna_university/EEE/5SEM/IC6501%20-%20CS/notes/105564_2013_regulation.pdf'),
(296, 'Control Systems Notes All Units', 'Control Systems Notes Unit  wise CS 6501', NULL, NULL, 'http://studentsfocus.com/notes/anna_university/EEE/5SEM/IC6501%20-%20CS/notes/IC6501(R-13)_uw_2013_regulation.pdf'),
(297, 'Control System CS QB 2m/16m with answers Unit 1', 'Control System CS QB 2m/16m with answers Unit 1\r\nControl Systems Notes QB Question Bank CS ic6501', NULL, NULL, 'http://studentsfocus.com/notes/anna_university/EEE/5SEM/IC6501%20-%20CS/2marks_16marks/QB105561_2013_regulation.pdf'),
(298, 'Control System CS QB 2m/16m with answers Unit 2', 'Control System CS QB 2m/16m with answers Unit \r\nCS Question Bank ic 6501', NULL, NULL, 'http://studentsfocus.com/notes/anna_university/EEE/5SEM/IC6501%20-%20CS/2marks_16marks/QB105562_2013_regulation.pdf'),
(299, 'Control System CS QB 2m/16m with answers Unit 3', 'Control System CS QB 2m/16m with answers Unit \r\nCS Question Bank ic 6501', NULL, NULL, 'http://studentsfocus.com/notes/anna_university/EEE/5SEM/IC6501%20-%20CS/2marks_16marks/QB105563_2013_regulation.pdf'),
(300, 'Control System CS QB 2m/16m with answers Unit  4', 'Control System CS QB 2m/16m with answers Unit \r\nCS Question Bank ic 6501', NULL, NULL, 'http://studentsfocus.com/notes/anna_university/EEE/5SEM/IC6501%20-%20CS/2marks_16marks/QB105564_2013_regulation.pdf'),
(301, 'Control System CS QB 2m/16m with answers All Unit', 'Control System CS QB 2m/16m with answers Unit \r\nCS Question Bank ic 6501', NULL, NULL, 'http://studentsfocus.com/notes/anna_university/EEE/5SEM/IC6501%20-%20CS/2marks_16marks/IC6501(R-13)_qb_2013_regulation.pdf'),
(302, 'Control System CS QB All Units', 'Control System CS QB 2m/16m with answers Unit \r\nCS Question Bank ic 6501', NULL, NULL, 'http://studentsfocus.com/notes/anna_university/EEE/5SEM/IC6501%20-%20CS/Imp_ques/IC6501-Control%20Systems%20Engineering_2013_regulation.pdf'),
(303, 'PC6505 Natural Gas Book', 'Advanced Natural Gas pc6505 Petro Chemical ', NULL, NULL, 'https://drive.google.com/open?id=17RgyfXl3RKTkP1XZCIbxgW2d6utbPlto');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `CID` int(11) NOT NULL,
  `BID` int(11) NOT NULL,
  `SID` int(11) NOT NULL,
  `COMM` varchar(150) NOT NULL,
  `LOGS` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`CID`, `BID`, `SID`, `COMM`, `LOGS`) VALUES
(5, 150, 6, 'good book', '2018-03-31'),
(6, 303, 6, 'how is the book', '2018-04-13');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `ID` int(200) NOT NULL,
  `SID` int(30) NOT NULL,
  `MESS` text NOT NULL,
  `LOGS` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`ID`, `SID`, `MESS`, `LOGS`) VALUES
(1, 6, 'hi da', '2018-03-31 13:32:37'),
(2, 3, 'how are you', '2018-03-31 13:32:58'),
(3, 3, 'how are you maaplas', '2018-03-31 08:15:09'),
(4, 5, 'SELECT NAME,MESS,LOGS FROM feedback INNER JOIN student ON student.ID = feedback.SID ORDER by LOGS DESC', '2018-03-31 08:16:10');

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE `request` (
  `RID` int(11) NOT NULL,
  `ID` int(11) NOT NULL,
  `TITLE` text NOT NULL,
  `AUTHOR` varchar(50) DEFAULT NULL,
  `CATEGORY` varchar(50) DEFAULT NULL,
  `LOGS` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`RID`, `ID`, `TITLE`, `AUTHOR`, `CATEGORY`, `LOGS`) VALUES
(1, 6, 'BOOKNAME', 'BOOKAUTHOR', 'Medical', '2018-04-13 09:47:01'),
(2, 6, 'Artificial Intelligence', 'J. Stanley JayaPrakash', 'School Books', '2018-04-13 10:09:24');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(150) NOT NULL,
  `GENDER` varchar(10) DEFAULT NULL,
  `PASS` varchar(150) NOT NULL,
  `MAIL` varchar(150) NOT NULL,
  `DEP` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`ID`, `NAME`, `GENDER`, `PASS`, `MAIL`, `DEP`) VALUES
(1, 'sivanesan', NULL, 'NzYzOTYyMDIyOA==', 'sivanesan220@gmail.com', 'CIVIL'),
(2, 'Silambarasan', NULL, 'U2lsYW1idTc=', 'smsilambu1@gmail.com', 'ECE'),
(3, 'KN Ramkumar', NULL, 'a25wcmFta3VtYXI=', 'knramkumar5@gmail.com', 'CIVIL'),
(4, 'pandi.j', NULL, 'cGFuZGl2YWlyYW0=', 'pandijp0004@gmail.com', 'CSE'),
(5, 'Karthick', NULL, 'a2FydGhpMDA4', 'karthickkumaar008@gmail.com', 'CSE'),
(6, 'Praveenram', NULL, 'cmFtamliYWJh', 'praveenrambalu@gmail.com', 'CSE'),
(7, 'Aswath', NULL, 'QXN3YXRoMTY=', 'Aswathmkp@gmail.com', 'CSE'),
(10, 'Giri', NULL, 'MTIzNDU1Njc4', 'vijaypraveen4554@gmail.com', 'CSE'),
(11, 'Arun', NULL, 'YXJ1bjE4MDY=', 'arunpandi1806@gmail.com', 'MECH'),
(12, 'LogeshRavi', NULL, 'a2F2aW5pbGE=', 'logeshravi1998@gmail.com', 'CSE'),
(13, 'Naveenkumar', NULL, 'bmF2ZWVuMTk5ODA2', 'naveenmech114@gmail.com', 'MECH'),
(17, 'Lovely Rajesh ', NULL, 'UmFqZXNoMTQz', 'rajeshcse1998@gmail.com', 'CSE'),
(20, 'Jenifer Angel Mary.G.A.', NULL, 'MjcwNjE5OTg=', 'jeniferangelmary98@gmail.com', 'ECE'),
(21, 'Sitendar Kumar nonia', NULL, 'JGl0ZW5kYXIxOQ==', 'sitendarchouhan@gmail.com', 'MECH'),
(22, 'Robin hood', NULL, 'cm9iaW50aGFsYQ==', 'robinhoodrock59@gmail.com', 'CIVIL'),
(23, 'muthulakshmi', NULL, 'YXJhdmluZGg5Nw==', 'muthulakshmiaravindh@gmail.com', 'CSE'),
(24, 'Sathish ', NULL, 'MTIzNDU2Nzg=', 'sathishs9k8@gmail.com', 'CSE'),
(25, 'Karthiksaravanan', NULL, 'MDAwMA==', 'karthiksaravanan.a@live.com', 'ECE'),
(26, 'Manikandan', NULL, 'ZGVlcGFrbWFuaQ==', 'deepakmanicse@gmail.com', 'CSE'),
(27, 'Vasuki kandasamy', NULL, 'YmlvbWVkaWNhbA==', 'vasuki7069@gmail.com', 'Bio Medical'),
(28, 'Ragav', NULL, 'ZWxpYnJhcnlAMiM=', 'ragav262@gmail.com', 'Bio Medical'),
(29, 'Suji', NULL, 'c3VqaQ==', 'sujisrini2015@gmail.com', 'Bio Medical'),
(30, 'Thameem ansari', NULL, 'RGhhcmFuaTc4Ng==', 'ansariperaiyur@gmail.com', 'IT'),
(31, 'Sathiyan ', NULL, 'c2F0aGl5YW4=', 'sathiyan086@gmail.com', 'CSE'),
(32, 'L.Karthick', NULL, 'NzM3MzIzNzM3Mg==', 'lmkarthick6@gmail.com', 'CIVIL'),
(33, 'Santhosh', NULL, 'c2FudGhvc2hzb3VuZGFyeWE=', 'santhoshss1005@gmail.com', 'Bio Medical'),
(34, 'Sathiesh', NULL, 'amVldml0aGE=', 'sathieshramasamy@gmail.com', 'CSE'),
(35, 'Udhayakumar', NULL, 'dWRoYXlkaGl2eWE=', 'budhayakumar801@gmail.com', 'CSE'),
(36, 'Manoj Agnesh', NULL, 'ODIyMDU2MTMxOA==', 'jbmanojagnesh96@gmail.com', 'EEE'),
(37, 'Nakkeeran ', NULL, 'YXVyZWthLm4=', 'sasikaranna235@gmail.com', 'CSE'),
(38, 'Bio priya', NULL, 'MjIwMg==', 'biopriya1998@gmail.com', 'Bio Medical'),
(39, 'Indumathi', NULL, 'QUxEUklORFU=', 'indunaveen10@gmail.com', 'Bio Medical'),
(40, 'Sathya sakthi', NULL, 'c2F0aHlhMTIz', 'sathyasakthi812@gmail.com', 'ECE'),
(41, 'Vasuki. S', NULL, 'dmFpc2h1', 'vaishnavikumar56@gmail.com', 'Bio Medical'),
(42, 'Elavarasi', NULL, 'ODcwMDYx', 'varshisri55@gmail.com', 'ECE'),
(43, 'Aravinthan', NULL, 'ODQyODM2ODEzNQ==', 'Mersalaravinth73@gmail.com', 'CSE'),
(44, 'mohamed gani', NULL, 'bWRnYW5p', 'mdgani114@gmail.com', 'MECH'),
(45, 'Prince', NULL, 'NzMzOTQ1NTIyNg==', 'Rockpraveen637@gmail.com', 'CSE'),
(46, 'M.GOWRISANKAR', NULL, 'TUFOSUtBTkRBTg==', 'gowrisiva341@gmail.com', 'EEE'),
(47, 'Balavenkatesh', NULL, 'YmFsYXYxOTk3', 'balavenkatesh1997@gmail.com', 'CSE'),
(48, 'Moganraj ', NULL, 'OTU5NzY0MDcxOQ==', 'mohanraj1995.civil@gmail.com', 'CIVIL'),
(49, 'Harish', NULL, 'aGFyaXNoZmFsY29u', 'harishfalcon400@gmail.com', 'CSE'),
(50, 'Aashif Ahmed ', NULL, 'YWFzaGlmYWhtZWQ5NjM=', 'aashif963@gmail.com', 'CIVIL'),
(51, 's pradeepa', NULL, 'a2FydGhpZGVlcGEyNDE0', 'spradeepabecse@gmail.com', 'CSE'),
(52, 'Manoj Kumar ', NULL, 'OTg0MzE3NDA0Nw==', 'manojkumaranmano@gmail.com', 'ECE'),
(55, 'Santhosh Kumar J', NULL, 'ODI0ODYyOTMxMA==', 'sashkumar11@gmail.com', 'CSE'),
(57, 'Mr Anthony Desmond Bsc. Eng (Hons)', NULL, 'RFdZLWRMSi14ZjgtcEZv', 'idesmond0303@gmail.com', 'CIVIL'),
(58, 'Giri Raja R', NULL, 'Z3Ryc3J2', 'giriravi395@gmail.com', 'ECE'),
(59, 'sasi', NULL, 'c2FzaWthcnRoaWNr', 'sasikarthick.ps001@gmail.com', 'CSE'),
(60, 'Prapanjan Karthick', NULL, 'cHJhcGFuamFuIDEx', 'karthickpmk1997@gmail.com', 'CSE'),
(61, 'Indrajith', NULL, 'aW5kcmFqaXRoMDIz', 'indrajithselvakumar@gmail.com', 'MECH'),
(62, 'SatheeshSakthi', NULL, 'c2F0aGVlc2hzYWt0aGk5MDc=', 'satheeshs299@gmail.com', 'CSE'),
(63, 'Arun', NULL, 'OTc4NjAxNDI0Mw==', 'rajarunraj418@Gmail.com', 'CSE'),
(64, 'suresh', NULL, 'YXNr', 'kesav071197@gmail.com', 'EEE'),
(65, 'M.sinthamani', NULL, 'MjMxMDE5OTg=', 'sindhususi23@gmail.com', 'EEE'),
(66, 'Karthick. R', NULL, 'c2l2YW5leW1hcg==', 'peterkarthi251@gmail.com', 'CSE'),
(67, 'Rashmi', NULL, 'cmFrc2hhbmFh', 'cibi27@yahoo.co.in', 'ECE'),
(68, 'Kavibharathi selvakumar', NULL, 'bGlicmFyeQ==', 'kavibharathi.selvakumar@gmail.com', 'MECH'),
(69, 'Koushika. P', NULL, 'c2hhcGVvZnU=', 'koushiswathipj57@gmail.com', 'EEE'),
(70, 'Prashanth', NULL, 'cHJhc2hhbnRoc2Vsdmk=', 'prashanthmurugesh997@gmail.com', 'IT'),
(71, 'Surya', NULL, 'cmVhbGhlcm8=', 'suryaprakash.elvino@gmail.com', 'MECH'),
(72, 'Yogesh', NULL, 'eW9nZXNoQDk4', 'yogeshcs1998@gmail.com', 'ECE'),
(75, 'Vignesh', NULL, 'MjAxMjE5OTQ=', 'vigneshrj20@gmail.com', 'CSE'),
(77, 'SANJAIBAL D', NULL, 'c2FuZ2VldGhhc2FuamFpYmFs', 'sanjaybal664@gmail.com', 'CSE'),
(78, 'Shagin banu', NULL, 'a3VqaWxpcHBh', '98shaginbanu@gmail.com', 'CSE'),
(79, 'MUNIS PRABHAAKAR', NULL, 'OTk0NDM4MDYyNg==', 'prabhajr2029@gmail.com', 'MECH'),
(80, 'Dhass', NULL, 'cmFtcHJha2FzaA==', 'dhassprakash5@gmail.com', 'CSE'),
(81, 'Harish', NULL, 'c3NhZGh1aGFyaQ==', 'harish1309159@gmail.com', 'MECH'),
(82, 'Nakkeeran ', NULL, 'c3VyZWthLm4=', 'Sasikaranns235@gmail.com', 'CSE'),
(83, 'Priya', NULL, 'cHJpeWFrYWxhaQ==', 'priyapearlk@gmail.com', 'CSE'),
(84, 'Ajaykarthi', NULL, 'OTk0MzE1NjIyNg==', 'ajaykarthi4334@gmail.com', 'IT'),
(85, 'Parthi ', NULL, 'cGFydGhpcGFu', 'parthikutta5599@gmail.com', 'CIVIL'),
(86, 'Vishal kumar', NULL, 'cXdlcnR5QDEyMw==', '9944436510v@gmail.com', 'Bio Medical'),
(87, 'Nataraj', NULL, 'bmF0YXJhajEwOTg=', 'nataraj1098@gmail.com', 'Bio Medical'),
(88, 'kumaravel ', NULL, 'a3VtYXJhdmVs', 'velkumar17798@gmail.com', 'CIVIL'),
(89, 'Srinivasan', NULL, 'bmFuZGh1c3Jpbmk=', 'srinimanoj2@gmail.com', 'ECE'),
(90, 'S.T.Gowtham', NULL, 'dGhhbmdhdmVsc3Q=', 'gthangavel07@gmail.com', 'MECH'),
(92, 'Sakthi Shabarish ', NULL, 'c2FrdGhpOTk5NDg=', 'sakthishabarish@gmail.com', 'MECH'),
(93, 'Shak', NULL, 'cXdlcnR5', 'shakhil1996@gmail.com', 'CSE'),
(94, 'Dhayanandan', NULL, 'a2FuaXNoa2FyMTQ=', 'rthayanand@gmail.com', 'MECH'),
(95, 'Sachin pushkar', NULL, 'c2FjaGlucHU=', 'sachin.pushkar.67@gmail.com', 'Bio Medical'),
(96, 'Sriram', NULL, 'c3JpcmFtMDY=', 'sriram060799@gmail.com', 'MECH'),
(97, 'Vishnu', NULL, 'OTY3NzkxNzc4NA==', 'vishnupriyan139@gmail.com', 'others'),
(98, 'Saravanakumar ', NULL, 'c2FyYXZhbmEyMTEy', 'saravana3666@gmail.com', 'CSE'),
(99, 'sammy', NULL, 'OTA4Nw==', 'periyasamy422@gmail.com', 'CIVIL'),
(100, 'Arvindraj Anguraju', NULL, 'YXJzYXZpbjcxMjMyMDAw', 'arvindraj3172@gmail.com', 'CSE'),
(101, 'Hemalatha.T', NULL, 'aGVtYWRldmE=', 'hemalathathiru102@gmail.com', 'Bio Medical'),
(106, 'Saranpandiyan Msp', NULL, 'bXNwaXBzMDA3', 'saranpandiyansp@gmail.com', 'CSE'),
(107, 'vijay', NULL, 'NzM5NzE=', 'rockvijay307@gmail.com', 'CSE'),
(109, 'M. Sowmiya ', NULL, 'OTU2NjM4MDk2NA==', 'sowmi1810@gmail.com', 'others'),
(111, 'Shakthi', NULL, 'c2FrdGhpdmVs', 'sakthivelcse09@gmail.com', 'CSE'),
(112, 'pavitra', NULL, 'OTk0NDEwNzAwNw==', 'pavitra14kum@gmail.com', 'Bio Medical'),
(113, 'Manikandan', NULL, 'OTA0Nzk4ODI1NQ==', 'mani99442@gmail.com', 'others'),
(114, 'Jegan', NULL, 'OTY1NTI3NDE3OQ==', 'mani240502@gmail.com', 'others'),
(116, 'kalaivanan', NULL, 'OTc5MTk3OTU5Mw==', 'kalaivanank14@gmail.com', 'others'),
(117, 'Pushparaj', NULL, 'NjI0NDUw', 'kalailife1993@gmail.com', 'others'),
(118, 'Dhilipkumar', NULL, 'NjExNjE1MTA0MTAx', 'dhilipan101@gmail.com', 'CSE'),
(119, 'Vinodhini', NULL, 'b3B0aW1pc3RpYw==', 'vinodhinitoptimistic@gmail.com', 'CSE'),
(120, 'anandhavel ', NULL, 'YXJ1bDE5OTc=', 'vathilingamamutha@gmail.com', 'EEE'),
(123, 'N. Kavya ', NULL, 'a2F2eWExNDM3', 'kavyakavya09639@gmail.com', 'ECE'),
(125, 'Nandhini', NULL, 'TmFuZGhpbmkxMjM=', 'nandhini052000@gmail.com', 'ECE'),
(128, 'Sivaraja', NULL, 'c2l2YXJhamEwNw==', 'msivarajacse@gmail.com', 'CSE'),
(129, 'Venkatesh', NULL, 'NzM3MzczNzM=', 'venkateshjeevanantham@gmail.com', 'ECE'),
(131, 'Murali', NULL, 'ODQ4OTk4NzE1NA==', 'murali15.saran@gmail.com', 'MECH'),
(132, 'Deepa', NULL, 'YW1tdQ==', 'deepatamil@gmail.com', 'EEE'),
(133, 'Prasanth', NULL, 'ZGFuZ2VyZHhhcm15ODI5', 'prasuzeno@gmail.com', 'ECE'),
(134, 'Sasikalau', NULL, 'YW1tdXNpbmdlcjgx', 'sasikalau81@gmail.com', 'ECE'),
(135, 'Shanmugam Mowgli', NULL, 'ODUwODQ0NjEwOA==', 'shanmugammowgli@gmail.com', 'CSE'),
(136, 'V.S.Aishwarya', NULL, 'YWlzaHUyOTA5', 'aishu.warya29@gmail.com', 'CSE'),
(137, 'Preetha.v', NULL, 'cHJlZXRoYTY2', 'preetha1997cse@gmail.com', 'CSE'),
(138, 'Nitul Sarma', NULL, 'bG92ZXlvdW1hQC45Nw==', 'sharmanitul.97@gmail.com', 'CSE'),
(139, 'Muniswaran.S', NULL, 'MTAxMDc4', 'munissubramaniyan@gmail.com', 'MECH'),
(140, 'Chandurukan', NULL, 'ODI0ODU3MjM=', 'chanduruchan777@gmail.com', 'CSE'),
(141, 'Suresh', NULL, 'NjExNjE1MTA0MDk2', 'srisuresh104@gmail.com', 'CSE'),
(142, 'Gopinath', NULL, 'cmFudWdvcGkxOTk3', 'gopinathkasi1997@gmail.com', 'CSE'),
(143, 'Sanjay', NULL, 'QVZFRUpOQVMzMg==', 'sanjeevasanjay234@gmail.com', 'ECE'),
(144, 'Andrew Paul', NULL, 'amVtaTAwMDExMQ==', 'andrewpauljemi88@gmail.com', 'CSE'),
(145, 'N.ramya ', NULL, 'bm5ycGZhbWlseSA=', 'ramyamarvellous02@gmail.com', 'CSE'),
(146, 'B.T.Tharani', NULL, 'dmVua2F0ZXNo', 'tharanibt97@gmail.com', 'Bio Medical'),
(147, 'RAJKUMAR', NULL, 'cmFqdXJhajgy', 'rajurajr057@gmail.com', 'CIVIL'),
(148, 'Kaleeswaran', NULL, 'NzA5MjQ1MjMzMQ==', 'kaleespooja027@gmail.com', 'CSE'),
(149, 'rabinraj', NULL, 'c3ViaW5yYWo=', 'rabinrabi26@gmail.com', 'CIVIL'),
(150, 'Aldrin stanly. C', NULL, 'cmFuaml0aGFt', 'stanleedurai97@gmail.com', 'CIVIL'),
(151, 'Shafeek Hasan', NULL, 'U2hhZmVlazEyMzQ=', 'shafeekhasan@gmail.com', 'others'),
(152, 'Suresh', NULL, 'c3VyZXNoY21zcw==', 'sureshsukumar001@gmail.com', 'MECH'),
(153, 'Karthik', NULL, 'eGthcnRoaWs=', 'karthikksivakami04@gmail.com', 'others'),
(154, 'Mohanraj ', NULL, 'ODIyMDYxNTk5MQ==', 'mohanrajb30698@gmail.com', 'CIVIL'),
(155, 'Sathish.P', NULL, 'c2F0aGlzaHN2bQ==', 'sathishpsvm227@gmail.com', 'CSE'),
(156, 'Vasanth', NULL, 'MTIzNDU2Nzg5MA==', 'vasanthchantru@gmail.com', 'MECH'),
(157, 'Vasanth', NULL, 'OTA4NzEwNTkyMg==', 'Valangaiprasanthvandaiyar123@gmail.Com', 'MECH'),
(158, 'Ragavendranbala', NULL, 'U1VHQXJhZ2FtNzg=', 'balasubramaniamragavendran78@gmail.com', 'others'),
(159, 'Ranjith Rk', NULL, 'cmFuaml2ZXJhamFsaXlhcg==', 'ranjivemani@gmail.com', 'EEE'),
(160, 'vinitha.s', NULL, 'VmluaXRoYQ==', 'smvinitha426@gmail.com', 'EEE'),
(162, 'Prabhudeva ', NULL, 'cGlsb3RERVZB', 'Prabhudevavm161198@gmail.com', 'others'),
(163, 'mani', NULL, 'bWFuaTY3OTc=', 'maniparamasivam67@gmail.com', 'CIVIL'),
(165, 'Gokul kannan', NULL, 'MjYxMjA0MDQ5OA==', 'gokulkannan2612@gmail.com', 'others'),
(166, 'Ramesh', NULL, 'OTA5NTc1MzEyMA==', 'kannanramesh098@gmail.com', 'CIVIL'),
(167, 'T.vignesh', NULL, 'OTU4NTAxOTgxNg==', 'sabrikrishna1@gmail.com', 'others'),
(168, 'Anand ', NULL, 'YW5hbmQyMjM=', 'anandkandiyar98@gmail.com', 'EEE'),
(169, 'G. BOOBALAN', NULL, 'cmFkaGE=', 'boobalang1998@gmail.com', 'MECH'),
(170, 'M.SABAREE', NULL, 'MTcxMTE5OTcy', 'sabareez17@gmail.com', 'others'),
(171, 'Anand', NULL, 'OTYyOTgxNjA0NA==', 'anandkandiyar98@gmail.com', 'EEE'),
(172, 'Suresh kumar', NULL, 'OTc4NzY3OTY1MA==', 'suremeigo@gmail.com', 'others'),
(173, 'Guru Prasanth M', NULL, 'c3ByYW1hbjEy', 'guruprasanthm61@gmail.com', 'MECH'),
(174, 'Shruthi Monika', NULL, 'YW1tdWJhYnk=', 'shruthimonika.r.eee@gmail.com', 'EEE'),
(175, 'Suthan ', NULL, 'c3JpQDEyMw==', 'srisuthan789@gmail.com', 'CSE'),
(176, 'Sathya chander', NULL, 'YXFlcGk0MzIx', 'sathyachanderb@gmail.com', 'EEE'),
(177, 'R.sridhar', NULL, 'c3JpZGhhcg==', 'sridhar24dsp@gmail.com', 'EEE'),
(178, 'P.venkatesh', NULL, 'a3V0dHkxMjM0', 'www.venkatesh.co.93@gmail.com', 'EEE'),
(179, 'Srikanth', NULL, 'ODc1NDg3ODA3Ng==', 'srikanthdheva36@gmail.com', 'EEE'),
(180, 'Jeevitha g', NULL, 'YXJ1bmVzaA==', 'Jeevithag1097@gmail.com', 'CSE'),
(181, 's.arun pandiyan', NULL, 'cmFqYXBhbmRp', 'arunbiomedical97@gmail.com', 'Bio Medical'),
(182, 'kavin N V', NULL, 'NzcwODg0MDQzNA==', 'kavinnv2808@gmail.com', 'Bio Medical'),
(183, 'Vinitha', NULL, 'ODY4MTA1MDU5OQ==', 'smvinitha1995@gmail.com', 'EEE'),
(184, 'RejinRathinam', NULL, 'NTEyOTc=', 'rejinrathinam97@gmail.com', 'others'),
(185, 'C SURAJIN KABIL DHAS ', NULL, 'MjMxMTIzMTE=', 'kabildhas5@gmail.com', 'others'),
(186, 'shalini', NULL, 'c2hhbGluaW1yc20=', 'shalinimrsm@gmail.com', 'MECH'),
(187, 'shalini', NULL, 'c2hhbGluaW1yc20=', 'shalinimrsm7@gmail.com', 'MECH'),
(188, 'Anbu', NULL, 'MTAyNTEwMjU=', 'anbazhagan1025@gmail.com', 'CSE'),
(189, 'Mohammed Kasim', NULL, 'bWhka2FzaW0=', 'sadamsafi98@gmail.com', 'MECH'),
(190, 'Krishnan', NULL, 'Y2tuY3NlMmsxMA==', 'ckrishnancse@gmail.com', 'CSE'),
(191, 'Swedha dharsini', NULL, 'Z2FuZXNoc3dl', 'swetharamesh68@gmail.com', 'ECE'),
(192, 'Backiyaraj V', NULL, 'OTg5NDAyMTgzMg==', 'backiyarajmannur@gmaail.com', 'EEE'),
(193, 'Arun Kumar', NULL, 'ODg4MzA0NDA1OA==', 'arunmasd47@gmail.com', 'EEE'),
(194, 'Arun Kumar', NULL, 'ODg4MzA0NDA1OA==', 'aruneeeboys@gmail.com', 'EEE'),
(195, 'Suriya ', NULL, 'ODEyNDQ2MTY2MA==', 'Suriyamannur1996@gmail.com', 'others'),
(196, 'Suriya', NULL, 'ODEyNDQ2MTY2MA==', 'Suriyamannur1996@email.com', 'others'),
(197, 'Ariharan', NULL, 'MjgwNDE5OTg=', 'ariharancse20@gmail.com', 'CSE'),
(198, 'Subraja.T', NULL, 'c3ViYnUwMDc=', 'shreesubraja98@gmail.com', 'CSE'),
(199, 'A. Abarna', NULL, 'YWJhcm5hMTIz', 'abarnaangamuthu98@gmail.com', 'ECE'),
(200, 'V. Sevvanthi', NULL, 'c2V2dmFudGhpMTIz', 'sevvanthivishwanathan19@gmail.com', 'EEE'),
(201, 'Elavarasan', NULL, 'OTc1MDQ3NTcxMw==', 'tkpela@gmail.com', 'EEE'),
(202, 'Manish', NULL, 'bWFuaWthbmRhbg==', 'manimathi3720@gamil.com', 'MECH'),
(203, 'S.BALACHANDAR', NULL, 'ZWx1bWFsYWkyMDE4', 'balachandar2015elu@gmail.com', 'MECH'),
(204, 'Others', NULL, 'ZWx1bWFsYWkyMDE4', 'neo777access1@gmail.com', 'others'),
(205, 'Karpagavalli', NULL, 'cmFuaXlhbW1hbA==', 'karpagavalli1111@gmail.com', 'IT'),
(206, 'Yogaraj D', NULL, 'WW9nYUAxMjM=', 'yogarajvys@gmail.com', 'MECH'),
(207, 'Praveen Ram', NULL, 'c2FuamF5', 'sanjays.selva@gmail.com', 'MECH'),
(208, 'Balamurugan', NULL, 'NTU1NjY2', 'balam4028@gmai.com', 'others'),
(209, 'V elaiyaraja ', NULL, 'c3VzaWRoaXZp', 'leafkingveerasv@gmail.com', 'others'),
(210, 'Chinnadurai R', NULL, 'ZHVyYWlAMTQz', 'chinnadurai603@gmail.com', 'others'),
(211, 'Vikram', NULL, 'ODY4MTA1MDU5OQ==', 'appumalar1997@gmail.com', 'EEE'),
(213, 'SATHISH KUMAR', 'Male', 'c2F0aGlzaGt1bWFy', 'sathish96594@gmail.com', 'CSE');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`AID`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`BID`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`CID`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`RID`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `AID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `BID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=305;

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `CID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `ID` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `request`
--
ALTER TABLE `request`
  MODIFY `RID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=214;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

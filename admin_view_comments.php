<?php
	include "database.php";
	include "function.php";
	session_start();
	if(!isset($_SESSION["AID"]))
	{
	header('Location: admin_login.php');

		// echo "<script>window.open('admin_login.php','_self')</script>";
	}
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
<?php include "head.php"; ?>
    </head>

    <body>
        <nav class="navbar navbar-default">
            <div class="">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mynavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>

            </button>
                    <a href="index.php" class="navbar-brand text-uppercase" style="color:white;">
                        <img src="img/logorect.png" class="img-responsive">
                    </a>
                </div>
                <div id="mynavbar" class="collapse navbar-collapse text-uppercase">
                    <ul class="nav navbar-nav navbar-right ">
					
                        <li><a href="" class="active">Hi...<?php echo $_SESSION["ANAME"]; ?></a></li>
                         
  <li><a href="admin_view_stud.php"><span class="fa fa-users"></span> Student</a></li>
  <li><a href="admin_add_book.php"> <span class="fa fa-upload"></span> Books</a></li>
  <li><a href="upload_books_link.php"> <span class="fa fa-cloud-upload"></span> Link</a></li>
  <li><a href="admin_view_book.php"><span class="fa fa-book"></span> Books</a></li>
  <li><a href="admin_view_req.php"><span class="fa fa-paper-plane"></span> Request</a></li>
  <li><a href="admin_view_comments.php"> <span class="fa fa-comments"></span> Comments</a></li>
                        <li><a href="admin_change.php"><span class="fa-passwd-reset fa-stack">
                                                         <i class="fa fa-undo fa-stack-2x"></i>
                                                            <i class="fa fa-lock fa-stack-1x"></i>
                                                                                        </span> Change Password</a></li>
                        <li><a href="logout.php"><span class="fa fa-power-off"></span> Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- body -->
        




<div class="container">
    <div class="row well outer">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <table class="table table-stripped text-white">


			<tr> <th>Total Students : </th><td> <?php echo countRecord("SELECT * from student",$db); ?></td> </tr>
			<tr>	<th>Total Books    : </th><td><?php echo countRecord("SELECT * from book",$db); ?></td> </tr>
			<tr> <th>Total Request  : </th><td><?php echo countRecord("SELECT * from request",$db); ?></td> </tr>
			<tr> <th>Total Comments : </th><td><?php echo countRecord("SELECT * from comment",$db); ?></td> </tr>
			<tr> <th>Activated accounts: </th><td><?php echo countRecord("SELECT * from student where STATE='ACTIVE'",$db); ?></td> </tr>
		</table>


        </div>
        <div class="col-sm-2"></div>
    </div>
</div>



<div class="container">
    <div class="row well outer">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
             <?php
							  if (isset($_GET["pg"])) {
								 $limit=$_GET["pg"];
								
							  }
							  else{
								  $limit=10;
							  }
							  ?>
                            <input type="hidden" value="<?php echo $limit; ?>" id="value">
            <?php 
$sql="SELECT  BTITLE,NAME,COMM,LOGS FROM comment INNER JOIN book ON book.BID = comment.BID INNER JOIN student ON comment.SID=student.ID LIMIT $limit";
		$res=$db->query($sql);
		if($res->num_rows>0)
		{
            echo '<div class="table-responsive">';
			echo "<table class='table table-stripped table-responsive'>";
					echo "<tr class='active'>";
						echo "<th>SNO</th>";
						echo "<th>BOOK</th>";
						echo "<th>STUDENT</th>";
						echo "<th>COMMENT</th>";
						echo "<th>LOGS</th>";
					echo "</tr>";
					$i=0;
				while($row=$res->fetch_assoc())
				{
					$i++;
					echo "<tr class='text-white'>";
					echo "<td>{$i}</td>";
					echo "<td>{$row["BTITLE"]}</td>";
					echo "<td style='font-weight:bold; font-style:italic;  text-transform:uppercase;'>{$row["NAME"]}</td>";
					echo "<td>{$row["COMM"]}</td>";
					echo "<td>{$row["LOGS"]}</td>";
					echo "</tr>";
				}
            echo "</table>";
            echo "</div>";
		}
		else
		{
            echo "<p class='text-danger'>No Book Record Found</p>";
		}
        ?>
        <?php echo '  <center><button id="loadmore" class="btn btn-default "><span class="fa fa-refresh"></span>  Load More</button> </center>'; ?>
        </div>
        <div class="col-sm-2"></div>
    </div>
</div>

  <?php
  if (isset($_GET["pg"])) {
    
     echo '<script>
     $("html, body").animate({ scrollTop: $(document).height() }, 10000);
     </script>';
  }
 
  ?>





   <nav class="navbar navbar-inverse">
       <center>
          <p class="text-white">Designed by <a href="https://facebook.com/praveenrambalu"> Praveenram Balachandran</a></p> 
       <img src="img/logorect.png" class="img img-responsive " style="max-height:100px; margin-bottom:20px;">

</center>
</nav>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js "></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js "></script>
        <script src="js/main.js "></script>
        <script>
                $("document").ready(function() {

                    $("#catediv,#catebtn").hide();
                    $("#catebtn").click(function() {
                    });
                     $("#loadmore").click(function() {
                        var value = $("#value").val();
                        var value2 = 10;
                        var newval = parseInt(value) + parseInt(value2);
                        $("#loadingvalue").val(newval);
                        $valuee = "admin_view_comments.php?pg=" + newval;
                        window.open($valuee, '_self')
                    });

                });
                //    window.open('index.php','_self')
            </script>
    </body>

    </html>

    </html>